int lengthOfLIS(int* nums, int numsSize)
{
    int* dp = (int*)malloc(sizeof(int) * numsSize);
    int i = 0, res = 1;
    for (i = 0; i < numsSize; i++)
    {
        dp[i] = 1;
    }
    for (i = 0; i < numsSize; i++)
    {
        int j = 0;
        for (j = 0; j < i; j++)
        {
            if (nums[i] > nums[j])
            
                dp[i] = fmax(dp[j] + 1, dp[i]);
            if (dp[i] > res)
                res = dp[i];
        }
    }
    return res;
}

int BinaryFind(int* dp, int left, int right, int target)
{
    while (left < right)
    {
        int mid = left + (right - left) / 2;
        if (dp[mid] > target)
            right = mid;
        else if (dp[mid] < target)
            left = mid + 1;
        else
            return mid;
    }
    return left;
}
int lengthOfLIS(int* nums, int numsSize)
{
    int* dp = (int*)malloc(sizeof(int) * numsSize);
    dp[0] = nums[0];
    int i = 1, count = 1;
    for (i = 1; i < numsSize; i++)
    {
        if (dp[count - 1] < nums[i])
        {
            dp[count++] = nums[i];
        }
        else
        {
            int index = BinaryFind(dp, 0, count - 1, nums[i]);
            if (dp[index] == nums[i])
                continue;
            if (dp[index] > nums[i])
                dp[index] = nums[i];
        }
    }
    return count;


}