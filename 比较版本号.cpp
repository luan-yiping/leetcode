class Solution {
public:
    int compareVersion(string a, string b)
    {
        int i = 0, j = 0;
        while (i < a.size() || j < b.size())
        {
            int x = i, y = j;
            while (x < a.size() && a[x] != '.') x++;
            while (y < b.size() && b[y] != '.') y++;
            int c = (i == x) ? 0 : atoi(a.substr(i, x - i).c_str());
            int d = (j == y) ? 0 : atoi(b.substr(j, y - j).c_str());
            if (c > d) return 1;
            if (c < d) return -1;

            i = x + 1;
            j = y + 1;
        }
        return 0;
    }
};