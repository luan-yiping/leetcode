class Solution
{
public:
	// 迭代
	int GetLength(ListNode* head)
	{
		int count = 0;
		while (head != nullptr)
		{
			count++;
			head = head->next;
		}
		return count;
	}
	ListNode* cut(ListNode* head, int step)
	{
		if (head == nullptr)
			return nullptr;
		for (int i = 1; i < step && head->next != nullptr; i++)
		{
			head = head->next;
		}
		ListNode* next = head->next;
		// 断链
		head->next = nullptr;
		return next;
	}
	ListNode* MergeTwoList(ListNode* head1, ListNode* head2)
	{
		ListNode* dummyHead = new ListNode(-1);
		dummyHead->next = nullptr;
		ListNode* tail = dummyHead;
		while (head1 && head2)
		{
			if (head1->val < head2->val)
			{
				tail->next = head1;
				tail = tail->next;
				head1 = head1->next;
			}
			else
			{
				tail->next = head2;
				tail = tail->next;
				head2 = head2->next;
			}
		}
		tail->next = head1 ? head1 : head2;
		return dummyHead->next;
	}
	ListNode* sortList(ListNode* head)
	{
		// 链表为空或只有一个结点,不需要合并
		if (head == nullptr || head->next == nullptr)
			return head;
		ListNode* dummyHead = new ListNode(-1);
		dummyHead->next = head;
		ListNode* tail = dummyHead;
		int step = 1;
		int length = GetLength(head);
		for (step = 1; step < length; step *= 2)
		{
			while (head != nullptr)
			{
				ListNode* h1 = head;
				ListNode* h2 = cut(h1, step);
				head = cut(h2, step);
				tail->next = MergeTwoList(h1, h2);
				while (tail->next != nullptr)
				{
					tail = tail->next;
				}
			}
			head = dummyHead->next;
			tail = dummyHead;
		}
		return dummyHead->next;
	}
	// 递归
	ListNode* sortList(ListNode* head)
	{
		if (head == nullptr || head->next == nullptr)
			return head;
		ListNode* slow = head, * fast = head->next;
		while (fast && fast->next)
		{
			fast = fast->next->next;
			slow = slow->next;
		}
		ListNode* rightHead = slow->next;
		// 断链
		slow->next = nullptr;
		ListNode* left = sortList(head);
		ListNode* right = sortList(rightHead);
		return MergeTwoList(left, right);
	}
    // 快排
	ListNode* QuickSort(ListNode* head, ListNode* end)
	{
		if (head == end || head->next == end)
			return head;
		ListNode* lhead = head, * utail = head, * p = head->next;
		while (p != end)
		{
			ListNode* next = p->next;
			if (p->val < head->val)
			{
				p->next = head;
				lhead = head;
			}
			else
			{
				utail->next = p;
				utail = p;
			}
			p = next;
		}
		utail->next = end;
		ListNode* node = QuickSort(lhead, head);
		head->next = QuickSort(head->next, end);
		return node;
	}
	ListNode* sortList(ListNode* head)
	{
		if (head == nullptr || head->next == nullptr)
			return head;
		return QuickSort(head,nullptr);
	}
};
