#include<iostream>
#include<cstring>
using namespace std;
const int N = 110;
typedef pair<int, int> PLL;
// g[N][N]用来存图,d[N][N]存每个点到起点的距离
int g[N][N], d[N][N];
// 存点坐标,pre[N][N]存每个点之前的点
PLL q[N * N], pre[N][N];
int n, m;
int bfs()
{
    memset(d, -1, sizeof d);
    d[0][0] = 0;
    q[0] = { 0,0 };
    int hh = 0, tt = 0;
    while (hh <= tt)
    {
        PLL t = q[hh++];
        int dx[4] = { -1,1,0,0 }, dy[4] = { 0,0,-1,1 };
        for (int i = 0; i < 4; i++)
        {
            int x = t.first + dx[i], y = t.second + dy[i];
            if (x >= 0 && x < n && y >= 0 && y < m && g[x][y] == 0 && d[x][y] == -1)
            {
                d[x][y] = d[t.first][t.second] + 1;
                pre[x][y] = t;
                q[++tt] = { x,y };
            }
        }
    }

    // 记录路径
    int x = n - 1, y = m - 1;
    while (x || y)
    {
        cout << x << " " << y << endl;
        auto t = pre[x][y];
        x = t.first, y = t.second;
    }

    return d[n - 1][m - 1];
}
int main()
{
    scanf("%d%d", &n, &m);
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            scanf("%d", &g[i][j]);
        }
    }
    printf("%d", bfs());
}