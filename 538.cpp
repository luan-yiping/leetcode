// leetcode 538
class Solution
{
public:
    int pre;
    void travelsal(TreeNode* root)
    {
        if (root == nullptr) return;
        travelsal(root->right);
        root->val += pre;
        pre = root->val;
        travelsal(root->left);
    }
    TreeNode* convertBST(TreeNode* root)
    {
        travelsal(root);
        return root;
    }
};