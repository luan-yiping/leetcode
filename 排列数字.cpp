#include<iostream>
using namespace std;
const int N = 10;
int st[N], tt, n;
bool flag[N];
void dfs()
{
    if (tt == n)
    {
        for (int i = 0; i < n; i++) printf("%d ", st[i]);
        printf("\n");
        return;
    }
    for (int i = 1; i <= n; i++)
    {
        if (!flag[i])
        {
            st[tt++] = i;
            flag[i] = true;
            dfs();
            tt--;
            flag[i] = false;
        }
    }
}
int main()
{
    scanf("%d", &n);
    dfs();
    return 0;
}