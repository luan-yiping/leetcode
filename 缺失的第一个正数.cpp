class Solution {
public:
    int firstMissingPositive(vector<int>& nums)
    {
        unordered_set<int> hash;
        for (auto e : nums) hash.insert(e);

        int res = 1;
        while (hash.count(res)) res++;
        return res;
    }
};