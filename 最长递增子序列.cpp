#include<iostream>
#include<vector>
using namespace std;
class Solution
{
public:
    int lengthOfLIS(vector<int>& nums)
    {
        // 动态规划
        /*
        int n = nums.size();
        vector<int> dp(n,1);
        for(int i = 1;i < n;i++)
        {
            for(int j = 0;j < i;j++)
            {
                if(nums[j] < nums[i]) dp[i] = max(dp[i],dp[j] + 1);
            }
        }
        int ret = 0;
        for(int i = 0;i < n;i++) ret = max(ret,dp[i]);
        return ret;
        */

        // 按字典序输出路径
        /*
        int n = nums.size();
        vector<int> dp(n,1);
        vector<int> path(n);
        for(int i = 0;i < n;i++)
        {
            path[i] = i;
            for(int j = 0;j < i;j++)
            {
               if (nums[j] < nums[i] && dp[j] + 1 > dp[i])
                {
                    dp[i] = dp[j] + 1;
                    path[i] = j;
                }
            }
        }
        int ret = 0,k = 0;
        for(int i = 0;i < n;i++)
        {
            if(dp[i] > ret)
            {
                ret = dp[i];
                k = i;
            }
        }
        vector<int> v;
        while(k != path[k])
        {
            v.push_back(nums[k]);
            k = path[k];
        }
        v.push_back(nums[k]);
        reverse(v.begin(),v.end());
        for(int i = 0;i < v.size();i++) cout<<v[i]<<endl;
        return ret;
        */

        // 二分
        vector<int> q;
        for (auto& e : nums)
        {
            if (q.empty() || e > q.back()) q.push_back(e);
            else
            {
                if (e <= q[0]) q[0] = e;
                else
                {
                    int l = 0, r = q.size() - 1;
                    while (l < r)
                    {
                        int mid = l + r + 1 >> 1;
                        if (q[mid] < e) l = mid;
                        else r = mid - 1;
                    }
                    q[l + 1] = e;
                }
            }
        }
        return q.size();
    }
};
int main()
{
    vector<int> nums{ 0,1,0,3,2,3 };
    Solution().lengthOfLIS(nums);
    return 0;
}