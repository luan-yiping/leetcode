class Solution
{
public:
    void dfs(TreeNode* root, vector<string>& ret, string path)
    {
        path += to_string(root->val);

        if (!root->left && !root->right)
        {
            ret.push_back(path);
            return;
        }

        if (root->left) dfs(root->left, ret, path + "->");
        if (root->right) dfs(root->right, ret, path + "->");
    }
    vector<string> binaryTreePaths(TreeNode* root)
    {
        vector<string> ret;
        if (root == nullptr) return ret;
        dfs(root, ret, "");
        return ret;
    }
};