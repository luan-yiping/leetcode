class Solution {
public:
    bool ipv4(string& ip)
    {
        int i = 0, j = 0, count = 0;
        while (j < ip.size())
        {
            while (j < ip.size() && ip[j] != '.')
            {
                if (ip[j] >= '0' && ip[j] <= '9') j++;
                else return false;
            }
            if (j < ip.size() && ip[j] == ':') return false;
            string t = ip.substr(i, j - i);
            if (t == "") return false;
            if (t[0] == '0' && t.size() > 1) return false;

            int ret = 0;
            for (auto& e : t)
            {
                ret = ret * 10 + (e - '0');
                if (ret > 255) return false;
            }

            if (j + 1 == ip.size()) return false;
            i = j + 1;
            j = i;
            count++;
        }
        if (count != 4) return false;
        return true;
    }
    bool ipv6(string& ip)
    {
        int i = 0, j = 0, count = 0;
        while (j < ip.size())
        {
            while (j < ip.size() && ip[j] != ':')
            {
                if ((ip[j] >= '0' && ip[j] <= '9') || (ip[j] >= 'a' && ip[j] <= 'f') || (ip[j] >= 'A' && ip[j] <= 'F')) j++;
                else return false;
            }
            if (j < ip.size() && ip[j] == ',') return false;
            string t = ip.substr(i, j - i);
            if (t.size() < 1 || t.size() > 4) return false;
            if (j + 1 == ip.size()) return false;
            i = j + 1;
            j = i;
            count++;
        }
        if (count != 8) return false;
        return true;
    }
    string validIPAddress(string queryIP)
    {
        if (ipv4(queryIP)) return "IPv4";
        if (ipv6(queryIP)) return "IPv6";
        return "Neither";
    }
};