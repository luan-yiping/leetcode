int removeElement(int* nums, int numsSize, int val)
{
    int cur = 0, ret = 0;
    while (cur < numsSize)
    {
        if (nums[cur] == val)
        {
            cur++;
        }
        else
        {
            nums[ret] = nums[cur];
            cur++;
            ret++;
        }
    }
    return ret;
}