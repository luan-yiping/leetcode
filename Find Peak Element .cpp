//为什么二分查找大的那一半一定会有峰值呢？（即nums[mid]<nums[mid + 1]时，mid + 1~N一定存在峰值） 
//首先已知 nums[mid + 1]>nums[mid]，
//那么mid + 2只有两种可能，一个是大于mid + 1，一个是小于mid + 1，小于mid + 1的情况，那么mid + 1就是峰值，
//大于mid + 1的情况，继续向右推，如果一直到数组的末尾都是大于的，那么可以肯定最后一个元素是峰值，
//因为nums[nums.length] = 负无穷
class Solution
{
public:
    int BinarySearch(vector<int>& nums)
    {
        int left = 0, right = nums.size() - 1;
        while (left < right)
        {
            int mid = left + (right - left) / 2;
            if ((nums[mid] > nums[mid + 1] && mid - 1 >= 0 && nums[mid] > nums[mid - 1]) || (nums[mid] > nums[mid + 1] && mid - 1 < 0))
            
                return mid;
            else if (nums[mid] < nums[mid + 1])
            
                left = mid + 1;
            else
                right = mid - 1;
        }
        return left;
    }
    int findPeakElement(vector<int>& nums)
    {
        return BinarySearch(nums);
    }
};