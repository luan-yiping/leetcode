typedef struct
{
    int* a;
    int k; // 队列长度
    int front; // 队头位置
    int tail; // 队尾的下一位置
} MyCircularQueue;

/*
多开一个空间的目的是为了在判断队列空还是满时防止判断条件一样
如 k = 3
1 2 3 front和tail起始都为0
队列为空时判断条件为 front == tail ,队列满时判断条件也为 front == tail

*/
MyCircularQueue* myCircularQueueCreate(int k) {
    MyCircularQueue* cq = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
    cq->a = (int*)malloc(sizeof(int) * (k + 1));
    cq->front = cq->tail = 0;
    
    cq->k = k;
    return cq;
}

bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
    return obj->front == obj->tail;
}

bool myCircularQueueIsFull(MyCircularQueue* obj) {
    int tailNext = obj->tail + 1;
    if (tailNext == obj->k + 1)
        tailNext = 0;
    return tailNext == obj->front;
}

bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {
    if (myCircularQueueIsFull(obj))
        return false;
    obj->a[obj->tail] = value;
    obj->tail++;
    // 让队列循环起来
    if (obj->tail == obj->k + 1)
        obj->tail = 0;
    return true;
}

bool myCircularQueueDeQueue(MyCircularQueue* obj) {
    if (myCircularQueueIsEmpty(obj))
        return false;
    ++obj->front;
    /*
    k = 3
    空     2    3   4
    tail  front
    当front到4时，再加 1 后越界了,将其置 0
    */
    if (obj->front == obj->k + 1)
        obj->front = 0;
    return true;
}

int myCircularQueueFront(MyCircularQueue* obj) {
    if (myCircularQueueIsEmpty(obj))
        return -1;
    return obj->a[obj->front];
}

/*
tail是指向队尾元素的下一位置的下标,因此应返回 obj->a[obj->tail - 1]
要注意的是
空     2    3   4
tail  front
此时应返回4,但obj->tail - 1为-1,应将obj->tail = obj->k
*/

int myCircularQueueRear(MyCircularQueue* obj) {
    if (myCircularQueueIsEmpty(obj))
        return -1;
    int tailPrev = obj->tail - 1;
    if (tailPrev == -1)
        tailPrev = obj->k;
    return obj->a[tailPrev];
}

// 先释放动态开辟的数组,再释放MyCircularQueue结点
void myCircularQueueFree(MyCircularQueue* obj) {
    free(obj->a);
    free(obj);
}

