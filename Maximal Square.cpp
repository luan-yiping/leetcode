class Solution
{
public:
    // 多开一行一列的空间是因为[["0","1"],["1","0"]]
    // 第一行第一列中存在最大值
    int maximalSquare(vector<vector<char>>& matrix)
    {
        vector<vector<int>> dp(matrix.size() + 1, vector<int>(matrix[0].size() + 1, 0));
        
        // row 行，col 列
        int row = matrix.size(), col = matrix[0].size(), ans = 0;
        for (int i = 1; i <= row; i++)
        {
            for (int j = 1; j <= col; j++)
            {
                if (matrix[i - 1][j - 1] == '1')
                    dp[i][j] = 1 + min(min(dp[i - 1][j - 1], dp[i][j - 1]), dp[i - 1][j]);
                ans = max(ans, dp[i][j]);
            }
        }
        return ans * ans;
    }
};

