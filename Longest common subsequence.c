int longestCommonSubsequence(char* text1, char* text2)
{
    int len1 = strlen(text1), len2 = strlen(text2);
    int** dp = (int**)malloc(sizeof(int*) * (len1 + 1));
    int k = 0;
    
    for (k = 0; k < len1 + 1; k++)
    {
        dp[k] = (int*)calloc((len2 + 1), sizeof(int));
    }
    int i = 1, max = 0;
    for (i = 1; i <= len1; i++)
    {
        int j = 1;
        for (j = 1; j <= len2; j++)
        {
            if (text1[i - 1] == text2[j - 1])
            {
                dp[i][j] = dp[i - 1][j - 1] + 1;
            }
            else
            {
                dp[i][j] = fmax(dp[i - 1][j], dp[i][j - 1]);
            }
            if (dp[i][j] > max)
                max = dp[i][j];
        }
    }
    return max;
}