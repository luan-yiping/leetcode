class Solution {
public:
    vector<string> ret;
    vector<string> generateParenthesis(int n)
    {
        dfs(n, 0, 0, "");
        return ret;
    }
    void dfs(int n, int lc, int rc, string seq)
    {
        if (lc == n && rc == n)
        {
            ret.push_back(seq);
            return;
        }
        if (lc < n) dfs(n, lc + 1, rc, seq + '(');
        if (rc < n && rc < lc) dfs(n, lc, rc + 1, seq + ')');
    }
};