void DFS(int* nums, int numsSize, int depth, int* path, bool* used, int** res, int* count)
{
    // ݹ鵽,ҵһ·
    
    if (depth == numsSize)
    {
        res[*count] = (int*)malloc(sizeof(int) * numsSize);
        memcpy(res[(*count)++], path, sizeof(int) * numsSize);
    }
    int i = 0;
    for (i = 0; i < numsSize; i++)
    {
        if (used[i] == true)
            continue;
        path[depth] = nums[i];
        
        used[i] = true;
        DFS(nums, numsSize, depth + 1, path, used, res, count);
        used[i] = false;
    }
}
int** permute(int* nums, int numsSize, int* returnSize, int** returnColumnSizes)
{
    *returnSize = 1;
    int i = 1;
    for (i = 1; i <= numsSize; i++)
        (*returnSize) *= i;
    *returnColumnSizes = (int*)malloc(sizeof(int) * (*returnSize));
    for (i = 0; i < (*returnSize); i++)
    {
        (*returnColumnSizes)[i] = numsSize;
    }
    int** res = (int**)malloc(sizeof(int*) * (*returnSize));
    int* path = (int*)malloc(sizeof(int) * numsSize);
    bool* used = (bool*)calloc(numsSize, sizeof(bool));
    int depth = 0;
    int count = 0;
    DFS(nums, numsSize, depth, path, used, res, &count);
    return res;
}