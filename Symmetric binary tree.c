typedef struct TreeNode TreeNode;
bool dfs(TreeNode* left, TreeNode* right)
{
	// 都为空
	if (left == NULL && right == NULL)
		return true;
	// 一个为空,一个不为空
	if (left == NULL || right == NULL)
		return false;
	// 都不为空,且值不相同
	if (left->val != right->val)
		return false;
	// 都不为空,且值相同
	return dfs(left->left, right->right) && dfs(left->right, right->left);
}
bool isSymmetric(struct TreeNode* root)
{
	// 树为空或只有一个结点
	if (root == NULL || (root->left == NULL && root->right == NULL))
		return true;
	return dfs(root->left, root->right);
}


// 迭代
struct TreeNode;
typedef struct TreeNode* QDataType;
typedef struct QueueNode
{
	QDataType data;
	struct QueueNode* next;
}QueueNode;
typedef struct Queue
{
	QueueNode* head;
	QueueNode* tail;
}Queue;
// 队列初始化
void QueueInit(Queue* pq)
{
	assert(pq);
	pq->head = pq->tail = NULL;
}
// 判空
bool QueueEmpty(Queue* pq)
{
	assert(pq);
	return pq->head == NULL;
}
// 入队
void QueuePush(Queue* pq, QDataType x)
{
	assert(pq);
	QueueNode* newNode = (QueueNode*)malloc(sizeof(QueueNode));
	if (newNode == NULL)
	{
		printf("malloc failed\n");
		exit(-1);
	}
	newNode->data = x;
	newNode->next = NULL;
	if (pq->head == NULL)
	{
		pq->head = pq->tail = newNode;
	}
	else
	{
		pq->tail->next = newNode;
		pq->tail = pq->tail->next;
	}
}
// 出队
void QueuePop(Queue* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));
	QueueNode* next = pq->head->next;
	free(pq->head);
	pq->head = next;
}
// 获取队尾元素
QDataType QueueBack(Queue* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));
	return pq->tail->data;
}
// 获取队头元素
QDataType QueueFront(Queue* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));
	return pq->head->data;
}
// 销毁队列
void QueueDestroy(Queue* pq)
{
	assert(pq);
	QueueNode* cur = pq->head;
	while (cur)
	{
		QueueNode* next = cur->next;
		free(cur);
		cur = next;
	}
}
// 队列元素个数
int QueueSize(Queue* pq)
{
	assert(pq);
	int count = 0;
	QueueNode* cur = pq->head;
	while (cur)
	{
		count++;
		cur = cur->next;
	}
	return count;
}
bool isSymmetric(struct TreeNode* root)
{
	if (root == NULL || (root->left == NULL && root->right == NULL))
		return true;
	Queue q;
	QueueInit(&q);
	QueuePush(&q, root->left);
	QueuePush(&q, root->right);
	while (!QueueEmpty(&q))
	{
		TreeNode* left = QueueFront(&q);
		QueuePop(&q);
		TreeNode* right = QueueFront(&q);
		QueuePop(&q);
		if (left == NULL && right == NULL)
			continue;
		if (left == NULL || right == NULL)
			return false;
		if (left->val != right->val)
			return false;
		QueuePush(&q, left->left);
		QueuePush(&q, right->right);
		QueuePush(&q, left->right);
		QueuePush(&q, right->left);
	}
	return true;
}