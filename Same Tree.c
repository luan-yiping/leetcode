typedef struct TreeNode TreeNode;
bool isSameTree(struct TreeNode* p, struct TreeNode* q)
{
    
    // 同时为空,返回true
    if (p == NULL && q == NULL)
        return true;
    // 一个为空,一个不为空,返回false
    if (p == NULL || q == NULL)
        return false;
    // 都不为空,且值不相同
    if (p->val != q->val)
        return false;
    // 都不为空，且值相同
    return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
}