class Solution {
public:
    void nextPermutation(vector<int>& nums)
    {
        int end = nums.size() - 2;
        while (end >= 0 && nums[end] >= nums[end + 1]) end--;
        if (end < 0)
        {
            reverse(nums.begin(), nums.end());
            return;
        }

        for (int i = nums.size() - 1; i > end; i--)
        {
            if (nums[i] > nums[end])
            {
                swap(nums[i], nums[end]);
                reverse(nums.begin() + end + 1, nums.end());
                return;
            }
        }
    }
};