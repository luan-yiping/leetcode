#include<iostream>
#include<vector>
using namespace std;
int main()
{
    int t;
    cin >> t;
    while (t--)
    {
        int n, k;
        cin >> n >> k;
        vector<int> left(100), right(100);
        vector<int> ans(200);
        for (int i = 0; i < n; i++) cin >> left[i];
        for (int i = 0; i < n; i++) cin >> right[i];
        while (k--)
        {
            for (int i = n - 1, j = 0; i >= 0; i--)
            {
                ans[j++] = right[i];
                ans[j++] = left[i];
            }
            reverse(ans.begin(), ans.begin() + 2 * n);
            for (int i = 0; i < n; i++) left[i] = ans[i];
            for (int i = 0, j = n; i < n; i++, j++) right[i] = ans[j];
        }
        for (int i = 0; i < 2 * n; i++) cout << ans[i] << " ";
        cout << endl;
    }
}