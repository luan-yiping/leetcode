class Solution {
public:
    bool wordBreak(string s, vector<string>& wordDict) {
        unordered_set<string> hash(wordDict.begin(), wordDict.end());

        int n = s.size();
        vector<bool> dp(n + 1, 0);
        dp[0] = true;

        for (int i = 1; i <= n; i++)
        {
            for (int j = 0; j < i; j++)
            {
                if (hash.find(s.substr(j, i - j)) != hash.end() && dp[j])
                {
                    dp[i] = true;
                    break;
                }
            }
        }

        return dp[n];
    }
};