typedef struct ListNode ListNode;

struct ListNode* deleteDuplicates(struct ListNode* head)
{
    if (head == NULL || head->next == NULL)
    {
        return head;
    }
    
    ListNode* prev = head, * cur = head->next;
    while (cur)
    
    {
        if (prev->val == cur->val)
        
        {
            prev->next = cur->next;
            free(cur);
            cur = prev->next;
            
        }
        else
        {
            prev = cur;
            cur = cur->next;
        }
    }
    return head;
}