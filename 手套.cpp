class Gloves
{
public:
    int findMinimum(int n, vector<int> left, vector<int> right)
    {
        int sum = 0, leftsum = 0, rightsum = 0, leftmin = 30, rightmin = 30;
        for (int i = 0; i < n; i++)
        {
            if (left[i] == 0 || right[i] == 0) sum += (left[i] + right[i]);
            else
            {
                leftsum += left[i];
                rightsum += right[i];
                leftmin = min(leftmin, left[i]);
                rightmin = min(rightmin, right[i]);
            }
        }
        return sum + min(leftsum - leftmin + 1, rightsum - rightmin + 1) + 1;
    }
};