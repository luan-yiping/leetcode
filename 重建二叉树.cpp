class Solution
{
public:
    TreeNode* reConstructBinaryTreeHelper(vector<int>& pre, int pre_start, int pre_end, vector<int>& vin, int vin_start, int vin_end)
    {
        // 无节点
        
        if (pre_start > pre_end)
            return nullptr;
        TreeNode* node = new TreeNode(pre[pre_start]);
        for (int i = vin_start; i <= vin_end; i++)
        {
            if (vin[i] == pre[pre_start])
            {
                node->left = reConstructBinaryTreeHelper(pre, pre_start + 1, pre_start + i - vin_start, vin, vin_start, i - 1);
                node->right = reConstructBinaryTreeHelper(pre, pre_start + i - vin_start + 1, pre_end, vin, i + 1, vin_end);
                break;
            }
        }
        return node;
    }
    TreeNode* reConstructBinaryTree(vector<int> pre, vector<int> vin)
    {
        if (pre.empty() || vin.empty())
            return nullptr;
        return reConstructBinaryTreeHelper(pre, 0, pre.size() - 1, vin, 0, vin.size() - 1);
    }
};