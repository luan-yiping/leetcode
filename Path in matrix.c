bool dfs(char** board, int x, int y,int row,int col,int k, char* word)
{
	if (x >= row || x < 0 || y >= col || y < 0 || board[x][y] != word[k])
		return false;
	if (k == strlen(word) - 1)
		return true;
	board[x][y] = '\0';
	bool res = (dfs(board, x + 1, y, row, col, k + 1, word) ||
		dfs(board, x - 1, y, row, col, k + 1, word) ||
		dfs(board, x, y + 1, row, col, k + 1, word) ||
		dfs(board, x, y - 1, row, col, k + 1, word));
	board[x][y] = word[k];
	return res;
}
bool exist(char** board, int boardSize, int* boardColSize, char* word)
{
	if (board == NULL || boardSize < 0 || word == NULL)
		return false;
	int i = 0;
	for (i = 0; i < boardSize; i++)
	{
		int j = 0;
		for (j = 0; j < *boardColSize; j++)
		{
			if (dfs(board, i, j, boardSize, *boardColSize, 0, word))
				return true;
		}
	}
	return false;
}