/* 方法一 :
(1).用head指针来标记有序序列的末尾
(2).使用prev指针从第一个结点开始循环和(head->next->val)相比，找到head->next->val插入的位置
*/
typedef struct ListNode ListNode;
struct ListNode* insertionSortList(struct ListNode* head)
{
    ListNode* dummyHead = (ListNode*)malloc(sizeof(ListNode));
    dummyHead->next = head;
    while (head && head->next)
    {
        // 找到有序序列的末尾
        if (head->val < head->next->val)
        
        
        {
            head = head->next;
            continue;
        }
        ListNode* prev = dummyHead;
        // 找到head->next->val插入的位置
        while (prev->next->val < head->next->val)
        {
            prev = prev->next;
        }
        // 在对应位置进行插入
        ListNode* tmp = head->next;
        head->next = tmp->next;
        tmp->next = prev->next;
        prev->next = tmp;
    }
    // 释放结点
    head = dummyHead->next;
    free(dummyHead);
    return head;
}

/* 方法二 :
(1).将head->next置为NULL,作为有序序列的第一个元素
(2).cur从剩下的元素遍历,同时prev和c指针用来判断cur的插入位置
*/

struct ListNode* insertionSortList(struct ListNode* head)
{
    if (head == NULL || head->next == NULL)
        return head;
    ListNode* dummyHead = (ListNode*)malloc(sizeof(ListNode));
    dummyHead->next = head;
    ListNode* cur = head->next, * prev = dummyHead, * c = head;
    head->next = NULL;
    while (cur)
    {
        ListNode* next = cur->next;
        // 判断插入位置
        while (c)
        {
            if (cur->val < c->val)
            {
                prev->next = cur;
                cur->next = c;
                cur = next;
                break;
            }
            else
            {
                prev = c;
                c = c->next;
            }
        }
        // 插入元素比当前有序序列最大的都大
        if (c == NULL)
        {
            prev->next = cur;
            cur->next = c;
            cur = next;
        }
        // 从头重新开始比较
        prev = dummyHead;
        c = dummyHead->next;
    }
    head = dummyHead->next;
    free(dummyHead);
    return head;
}
