class Solution
{
public:
    static bool cmp(int x, int y)
    {
        long sx = 10, sy = 10;
        while (sx <= x)
            sx *= 10;
        while (sy <= y)
            sy *= 10;
        return x * sy + y > y * sx + x;
    }
    string largestNumber(vector<int>& nums)
    {
        sort(nums.begin(), nums.end(), cmp);
        string ret;
        for (auto& e : nums)
        {
            if (!(e == 0 && ret[0] == '0'))
                ret += to_string(e);
        }
        return ret;
    
};