class Solution {
public:
    string removeKdigits(string num, int k) {
        stack<char> st;

        for (int i = 0; i < num.size(); i++)
        {
            while (k > 0 && !st.empty() && num[i] < st.top())
            {
                st.pop();
                k--;
            }
            if (num[i] != '0' || !st.empty()) st.push(num[i]);
        }

        while (k)
        {
            if (!st.empty()) st.pop();
            k--;
        }

        if (st.empty()) return "0";

        string res;
        while (st.size())
        {
            res += st.top();
            st.pop();
        }
        reverse(res.begin(), res.end());
        return res;
    }
};