class Solution
{
public:
    int trap(vector<int>& height)
    {

        // 双指针
        int size = height.size();
        int sum = 0;
        for (int i = 1; i < size - 1; i++)
        {
            int maxleft = height[i];
            
            int maxright = height[i];
            for (int left = i - 1; left >= 0; left--)
            {
                if (height[left] > maxleft)
                    maxleft = height[left];
            }
            for (int right = i + 1; right < size; right++)
            {
                if (height[right] > maxright)
                    maxright = height[right];
            
            sum += min(maxleft, maxright) - height[i];
        }
        return sum;
    }


    // 动态规划
    int size = height.size();
    if (size == 0)
        return 0;
    int sum = 0;
    // 从左向右遍历
    vector<int> maxleftarray(size);
    // 从右向左遍历
    vector<int> maxrightarray(size);
    maxleftarray[0] = height[0];
    maxrightarray[size - 1] = height[size - 1];
    for (int i = 1; i < size; i++)
    {
        if (height[i] > maxleftarray[i - 1])
            maxleftarray[i] = height[i];
        else
            maxleftarray[i] = maxleftarray[i - 1];
    }
    for (int i = size - 2; i >= 1; i--)
    {
        if (height[i] > maxrightarray[i + 1])
            maxrightarray[i] = height[i];
        else
            maxrightarray[i] = maxrightarray[i + 1];
    }
    for (int i = 1; i < size - 1; i++)
    {
        int maxleft = maxleftarray[i];
        int maxright = maxrightarray[i];
        sum += min(maxleft, maxright) - height[i];
    }
    return sum;

    // 单调栈
    // 栈中存放下标
    stack<int> st;
    int size = height.size();
    int sum = 0;
    if (size == 0)
        return 0;
    st.push(0);
    for (int i = 1; i < size; i++)
    {
        if (height[i] < height[st.top()])
        {
            st.push(i);
        }
        else if (height[i] == height[st.top()])
        {
            st.pop();
            st.push(i);
        }
        // height[i] > height[st.top()]
        else
        {
            while (!st.empty() && height[i] > height[st.top()])
            {
                int tmp = st.top();
                st.pop();
                if (!st.empty())
                    sum += (min(height[i], height[st.top()]) - height[tmp]) * (i - st.top() - 1);
            }
            st.push(i);
        }
    }
    return sum;
}


};