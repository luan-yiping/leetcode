class Solution {
public:
    int majorityElement(vector<int>& nums)
    {
        int ret = nums[0], cnt = 1;
        for (int i = 1; i < nums.size(); i++)
        {
            if (nums[i] != ret)
            {
                cnt--;
                if (cnt == 0)
                {
                    ret = nums[i];
                    cnt = 1;
                }
            }
            else
            {
                cnt++;
            }
        }
        return ret;
    }
};