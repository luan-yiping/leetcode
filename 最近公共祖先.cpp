#include<iostream>
using namespace std;
class LCA
{
public:
    int getLCA(int a, int b)
    {
        if (a / 2 == b)
            return b;
        if (b / 2 == a)
            return a;
        int p1 = a / 2, p2 = b / 2;
        while (p1 != p2)
        {
            if (p1 > p2)
                p1 /= 2;
            else
                p2 /= 2;
        }
        return p1;
    }
};
int main()
{
    LCA a;
    cout << a.getLCA(9, 7) << endl;
}