#include<iostream>
using namespace std;
int f(int n)
{
    int sum = 0;
    while (n)
    {
        sum += (n % 10);
        n /= 10;
    }
    return sum;
}
int main()
{
    string s;
    while (cin >> s)
    {
        int ret = 0;
        for (int i = 0; i < s.size(); i++) ret += s[i] - '0';
        while (ret >= 10)
        {
            ret = f(ret);
        }
        cout << ret << endl;
    }
}