typedef struct ListNode ListNode;
// 从中间结点开始去反转链表
ListNode* reverse(ListNode* mid)
{
    ListNode* n1 = NULL, * n2 = mid, * n3 = n2->next;
    while (n2)
    {
        n2->next = n1;
        n1 = n2;
        n2 = n3;
        if (n2)
        {
            n3 = n2->next;
        }
    }
    return n1;
}
bool isPalindrome(struct ListNode* head)
{
    if (head == NULL)
        return false;
    ListNode* fast = head, * slow = head;
    while (fast && fast->next)
    {
        fast = fast->next->next;
        slow = slow->next;
    }
    ListNode* mid = reverse(slow);
    // 判断中间结点之前的链表和从中间结点反转后的链表是否相同
    while (head && mid)
    {
        if (head->val != mid->val)
        {
            return false;
        }
        else
        {
            head = head->next;
            mid = mid->next;
        }
    }
    return true;
}