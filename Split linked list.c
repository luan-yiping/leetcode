// 将小于x的结点连接成一条链表,将大于等于x的结点连接成一条链表,将两条链表合并到一起
// 注意最后要将 greaterTail->next置成 NULL,否则会死循环(牛客报错 : 超出内存限制)
class Partition

{
public:
    ListNode* partition(ListNode* pHead, int x)
    {
        if (pHead == NULL)
            return NULL;
        ListNode* lessHead, * greaterHead, * lessTail, * greaterTail;
        lessHead = lessTail = (ListNode*)malloc(sizeof(ListNode));
        greaterHead = greaterTail = (ListNode*)malloc(sizeof(ListNode));
        lessHead->next = NULL;
        greaterHead->next = NULL;
        ListNode* cur = pHead;
        while (cur != NULL)
        {
            if (cur->val < x)
            {
                8
                lessTail->next = cur;
                lessTail = lessTail->next;
            }
            else
            {
                greaterTail->next = cur;
                greaterTail = greaterTail->next;
            }
            cur = cur->next;
        }
        greaterTail->next = NULL;
        lessTail->next = greaterHead->next;
        pHead = lessHead->next;
        free(lessHead);
        free(greaterHead);
        return pHead;
    }
};