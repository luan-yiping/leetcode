class Solution
{
public:
    bool backspaceCompare(string s, string t)
    {
        int snum = 0, tnum = 0;
        int i = s.size() - 1;
        int j = t.size() - 1;
        while (1)
        {
            while (i >= 0)
            {
                if (s[i] == '#') snum++;
                else
                {
                    if (snum > 0) snum--;
                    else break;
                }
                i--;
            }
            while (j >= 0)
            {
                if (t[j] == '#') tnum++;
                else
                {
                    if (tnum > 0) tnum--;
                    else break;
                }
                j--;
            }
            if (i == -1 || j == -1) break;
            if (s[i] != t[j]) return false;
            i--;
            j--;
        }
        if (i == -1 && j == -1) return true;
        else return false;
    }
};