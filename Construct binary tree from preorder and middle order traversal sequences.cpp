typedef struct TreeNode TreeNode;
int FindIndex(int* inorder, int inorderSize, int target)

{
    for (int i = 0; i < inorderSize; i++)
    {
        if (inorder[i] == target)
        {
            return i;
        }
    }
    return -1;
}
TreeNode* mybuildTree(int* preorder, int* inorder, int inorderSize, int preleft, int preright, int inleft, int inright)
{
    if (preleft > preright)
        return NULL;
        
    // 前序遍历列表中根的下标,中序遍历列表中根的下标
    int preroot = preleft, inroot = FindIndex(inorder, inorderSize, preorder[preroot]);
    // 根的左子树的结点个数
    int size = inroot - inleft;
    TreeNode* root = (TreeNode*)malloc(sizeof(TreeNode));
    root->val = preorder[preroot];
    root->left = mybuildTree(preorder, inorder, inorderSize, preleft + 1, preleft + size, inleft, inroot - 1);
    root->right = mybuildTree(preorder, inorder, inorderSize, preleft + size + 1, preright, inroot + 1, inright);
    return root;

}
struct TreeNode* buildTree(int* preorder, int preorderSize, int* inorder, int inorderSize)
{
    return mybuildTree(preorder, inorder, inorderSize, 0, preorderSize - 1, 0, inorderSize - 1);
}



// 迭代
class Solution
{
public:
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder)
    {
        TreeNode* root = new TreeNode(preorder[0]);
        stack<TreeNode*>st;
        st.push(root);
        int inorderIndex = 0;
        for(int i = 1;i < preorder.size();i++)
        {
            if(st.top()->val != inorder[inorderIndex])
            {
                TreeNode* node = new TreeNode(preorder[i]);
                st.top()->left = node;
                st.push(node);
            }
            else
            {
                TreeNode* node = nullptr;
                while(!st.empty() && st.top()->val == inorder[inorderIndex])
                {
                    node = st.top();
                    st.pop();
                    inorderIndex++;
                }
                node->right = new TreeNode(preorder[i]);
                st.push(node->right);
            }
        }
        return root;
    }
};