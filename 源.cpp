#include<iostream>
using namespace std;
class HeapOnly
{
public:
	static HeapOnly* CreateObj()
	{
		return new HeapOnly();
	}
private:
	HeapOnly();
	HeapOnly(const HeapOnly&);
	// HeapOnly(const HeapOnly&) = delete;
};
int main()
{
	HeapOnly* p = HeapOnly::CreateObj();
}