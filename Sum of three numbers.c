int cmp(const void* e1, const void* e2)
{
    return *(int*)e1 - *(int*)e2;
}
int** threeSum(int* nums, int numsSize, int* returnSize, int** returnColumnSizes)
{
    if (nums == NULL || numsSize < 3)
    {
        *returnSize = 0;
        return NULL;
    }
    qsort(nums, numsSize, sizeof(int), cmp);
    int** ans = (int**)malloc(sizeof(int*) * (numsSize) * (numsSize));
    *returnColumnSizes = (int*)malloc(sizeof(int) * (numsSize) * (numsSize));
    int i = 0;
    for (i = 0; i < (numsSize) * (numsSize); i++)
    {
        (*returnColumnSizes)[i] = 3;
    }
    *returnSize = 0;
    int k = 0;
    for (k = 0; k < numsSize - 2; k++)
    {
        if (nums[k] > 0)
            break;
        if (k > 0 && nums[k] == nums[k - 1])
            continue;
        int i = k + 1, j = numsSize - 1;
        while (i < j)
        {
            int sum = nums[k] + nums[i] + nums[j];
            if (sum < 0)
            {
                i++;
                while (i < j && nums[i] == nums[i - 1])
                    i++;
            }
            else if (sum > 0)
            {
                j--;
                while (i < j && nums[j] == nums[j + 1])
                    j--;
            }
            else
            {
                int* ret = (int*)malloc(sizeof(int) * 3);
                ret[0] = nums[k];
                ret[1] = nums[i];
                ret[2] = nums[j];
                ans[(*returnSize)++] = ret;
                i++;
                j--;
                while (i < j && nums[i] == nums[i - 1])
                    i++;
                while (i < j && nums[j] == nums[j + 1])
                    j--;
            }
        }
    }
    return ans;
}