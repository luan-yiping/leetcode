lass Solution
{
public:
    int search(vector<int> & nums, int target)
    {
        int left = 0,right = nums.size() - 1;
        while (left < right)
        {
            int mid = left + (right - left + 1) / 2;
            // [mid,right]����
            if (nums[mid] < nums[right])
            {
                if (target >= nums[mid] && target <= nums[right])
                    left = mid;
                else
                    right = mid - 1;
            }
            // [left,mid]����
            else
            {
                if (target >= nums[left] && target <= nums[mid - 1])
                    right = mid - 1;
                else
                    left = mid;
            }
        }
        if (nums[left] == target)
            return left;
        else
            return -1;
    }
};
