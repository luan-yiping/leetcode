class Solution {
public:
    int widthOfBinaryTree(TreeNode* root)
    {
        if (!root) return 0;

        queue<pair<TreeNode*, long long>> q;
        q.push({ root,1 });
        long long res = 0;
        while (q.size())
        {
            int n = q.size();
            long long l = q.front().second, r;
            for (int i = 0; i < n; i++)
            {
                auto p = q.front();
                q.pop();
                long long v = p.second - l + 1;
                r = p.second;
                if (p.first->left) q.push({ p.first->left,v * 2 });
                if (p.first->right) q.push({ p.first->right,v * 2 + 1 });
            }
            res = max(res, r - l + 1);
        }

        return res;
    }
};

