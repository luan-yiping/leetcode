class Solution
{
public:
    int totalFruit(vector<int>& fruits)
    {
        int res = 0;
        unordered_map<int, int> m;
        for (int i = 0, j = 0, s = 0; j < fruits.size(); j++)
        {
            if (++m[fruits[j]] == 1) s++;
            while (s > 2)
            {
                if (--m[fruits[i]] == 0) s--;
                i++;
            }
            res = max(res, j - i + 1);
        }
        return res;
    }
};