class Solution
{
public:
    vector<int> searchRange(vector<int>& nums, int target)
    
    {
        vector<int> res(2, -1);
        if (nums.empty())
            return res;
        int left = 0, right = nums.size() - 1;
        // 找到目标值在数组第一次出现的位置
        while (left < right)
        {
            int mid = left + (right - left) / 2;
            if (nums[mid] < target)
            {
                
                left = mid + 1;
            }
            else if (nums[mid] > target)
            {
                right = mid - 1;
            }
            else
            {
                right = mid;
            }
        }
        if (nums[left] == target)
            res[0] = left;
        else
            return res;
        // 找到目标值在数组最后一次出现的位置,此时left指向数组第一次出现的位置
        right = nums.size() - 1;
        while (left < right)
        {
            int mid = left + (right - left + 1) / 2;
            if (nums[mid] > target)
            {
                right = mid - 1;
            }
            else
            {
                left = mid;
            }
        }
        if (nums[left] == target)
            res[1] = left;


        return res;
    }
};