class CQueue {
public:
    stack<int> st1;
    stack<int> st2;
    CQueue() {

    }

    void appendTail(int value) {
        st1.push(value);
    }

    int deleteHead() {
        if (st2.empty())
        {
            if (st1.empty()) return -1;
            while (!st1.empty())
            {
                st2.push(st1.top());
                st1.pop();
            }
            int ret = st2.top();
            st2.pop();
            return ret;
        }
        else
        {
            int ret = st2.top();
            st2.pop();
            return ret;
        }
    }
};
