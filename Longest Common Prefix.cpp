class Solution
{
public:
    string longestCommonPrefix(vector<string>& strs)
    {
        //int minsize = strs[0].size();
        //for(int i = 0;i < strs.size();i++)
        //{
        //    if(strs[i].size() < minsize)
        //        minsize = strs[i].size();
        //}
        int ans = 0;
        for (int i = 0; i < strs[0].size(); i++)
    
        {
            for (int j = 0; j < strs.size() - 1; j++)
            
            {
                if (i >= strs[j].size() || strs[j][i] != strs[j + 1][i])
                    return strs[0].substr(0, ans);
            }
            ans += 1;
        }
        return strs[0].substr(0, ans);
    }
};
