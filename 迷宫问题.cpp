#include<iostream>
#include<vector>
using namespace std;
typedef pair<int, int> PII;
int n, m;
void dfs(vector<PII>& ans, vector<vector<int>>& v,
    vector<vector<bool>>& b, int i, int j)
{
    if (i == n - 1 && j == m - 1)
    {
        ans.push_back(make_pair(i, j));
        for (int i = 0; i < ans.size(); i++)
        {
            cout << "(" << ans[i].first << "," << ans[i].second << ")" << endl;
        }
        return;
    }
    ans.push_back(make_pair(i, j));
    b[i][j] = 1;
    if (i + 1 < n && v[i + 1][j] == 0 && b[i + 1][j] == 0) dfs(ans, v, b, i + 1, j);
    if (j + 1 < m && v[i][j + 1] == 0 && b[i][j + 1] == 0) dfs(ans, v, b, i, j + 1);
    if (i - 1 >= 0 && v[i - 1][j] == 0 && b[i - 1][j] == 0) dfs(ans, v, b, i - 1, j);
    if (j - 1 >= 0 && v[i][j - 1] == 0 && b[i][j - 1] == 0) dfs(ans, v, b, i, j - 1);

    ans.pop_back();
}
int main()
{
    while (cin >> n >> m)
    {
        vector<vector<int>> v(n, vector<int>(m, 0));
        vector<vector<bool>> b(n, vector<bool>(m, 0));
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                cin >> v[i][j];
            }
        }
        vector<PII> ans;
        dfs(ans, v, b, 0, 0);
    }
    return 0;
}