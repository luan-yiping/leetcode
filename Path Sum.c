#define MAX 5000
typedef struct TreeNode TreeNode;
void dfs(TreeNode* node, int* path, int** ret, int* sum, int* nodesum, int targetSum, int* returnSize, int** returnColumnSizes)
{
    if (node == NULL)
        return;
    // 将节点放入路径数组中
    path[(*nodesum)++] = node->val;
    *sum += node->val;
    // 若等于给定目标和,且当前节点为叶子节点
    if (*sum == targetSum && node->left == NULL && node->right == NULL)
    {
        // 将路径拷贝到返回数组中
        ret[*returnSize] = (int*)malloc(sizeof(int) * (*nodesum));
        for (int i = 0; i < *nodesum; i++)
        {
            ret[*returnSize][i] = path[i];
        }
        (*returnColumnSizes)[*returnSize] = *nodesum;
        (*returnSize)++;
    }
    // 递归左
    dfs(node->left, path, ret, sum, nodesum, targetSum, returnSize, returnColumnSizes);
    // 递归右
    dfs(node->right, path, ret, sum, nodesum, targetSum, returnSize, returnColumnSizes);
    // 走到叶子节点了,减掉当前节点的值,回溯到上一层递归
    (*nodesum)--;
    *sum -= node->val;
}
int** pathSum(struct TreeNode* root, int targetSum, int* returnSize, int** returnColumnSizes)
{
    *returnSize = 0;
    // ret为返回的结果数组
    int** ret = (int**)calloc(MAX, sizeof(int*));
    // 每行的数据个数
    *returnColumnSizes = (int*)malloc(sizeof(int) * MAX);
    // 记录路径
    int* path = (int*)malloc(sizeof(int) * MAX);
    // sum为路径和,nodesum为路径节点数
    int sum = 0, nodesum = 0;
    dfs(root, path, ret, &sum, &nodesum, targetSum, returnSize, returnColumnSizes);
    return ret;
}