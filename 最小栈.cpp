class MinStack {
public:
    stack<int> st;
    MinStack() {

    }

    void push(int val)
    {
        if (st.empty())
        {
            st.push(val);
            st.push(val);
        }
        else
        {
            int tmp = st.top();
            st.push(val);
            st.push(val < tmp ? val : tmp);
        }
    }

    void pop() {
        st.pop();
        st.pop();
    }

    int top() {
        int tmp = st.top();
        // cout<<tmp<<endl; // -2
        st.pop();
        int ret = st.top(); // 0
        // cout<<ret<<endl;
        st.push(tmp);
        return ret;
    }

    int getMin() {
        return st.top();
    }
};

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack* obj = new MinStack();
 * obj->push(val);
 * obj->pop();
 * int param_3 = obj->top();
 * int param_4 = obj->getMin();
 */