typedef int QDataType;
typedef struct QueueNode
{
	QDataType data;
	struct QueueNode* next;
}QueueNode;
typedef struct Queue
{
	QueueNode* head;
	QueueNode* tail;
}Queue;
// 队列初始化
void QueueInit(Queue* pq)
{
	assert(pq);
	pq->head = pq->tail = NULL;
}
// 入队
void QueuePush(Queue* pq, QDataType x)
{
	assert(pq);
	QueueNode* newNode = (QueueNode*)malloc(sizeof(QueueNode));
	if (newNode == NULL)
	{
		printf("malloc failed\n");
		exit(-1);
	}
	newNode->data = x;
	newNode->next = NULL;
	if (pq->head == NULL)
	{
		pq->head = pq->tail = newNode;
	}
	else
	{
		pq->tail->next = newNode;
		pq->tail = pq->tail->next;
	}
}
// 判空
bool QueueEmpty(Queue* pq)
{
	assert(pq);
	return pq->head == NULL;
}
// 出队
void QueuePop(Queue* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));
	QueueNode* next = pq->head->next;
	free(pq->head);
	pq->head = next;
}
// 获取队尾元素
QDataType QueueBack(Queue* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));
	return pq->tail->data;
}
// 获取队头元素
QDataType QueueFront(Queue* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));
	return pq->head->data;
}
// 销毁队列
void QueueDestroy(Queue* pq)
{
	assert(pq);
	QueueNode* cur = pq->head;
	while (cur)
	{
		QueueNode* next = cur->next;
		free(cur);
		cur = next;
	}
}
// 队列元素个数
int QueueSize(Queue* pq)
{
	assert(pq);
	int count = 0;
	QueueNode* cur = pq->head;
	while (cur)
	{
		count++;
		cur = cur->next;
	}
	return count;
}
typedef struct {
	Queue q1;
	Queue q2;
} MyStack;

// 将两个队列进行初始化
MyStack* myStackCreate()
{
	MyStack* st = (MyStack*)malloc(sizeof(MyStack));
	QueueInit(&st->q1);
	QueueInit(&st->q2);
	return st;
}

// 让一个队列入数据，另一个队列为空,向不为空的队列里入数据
void myStackPush(MyStack* obj, int x)
{
	// q1为空
	if (QueueEmpty(&obj->q1))
	{
		QueuePush(&obj->q2, x);
	}
	else
	{
		QueuePush(&obj->q1, x);
	}
}

//将数据转到空队列里，返回并删除最后一个元素
int myStackPop(MyStack* obj)
{
	// q1为空
	if (QueueEmpty(&obj->q1))
	{
		int len = QueueSize(&obj->q2);
		while (--len)
		{
			QueuePush(&obj->q1, ((obj->q2).head)->data);
			QueuePop(&obj->q2);
		}
		int tmp = ((obj->q2).head)->data;
		QueuePop(&obj->q2);
		return tmp;
	}
	// q2为空
	else
	{
		int len = QueueSize(&obj->q1);
		while (--len)
		{
			QueuePush(&obj->q2, ((obj->q1).head)->data);
			QueuePop(&obj->q1);
		}
		int tmp = ((obj->q1).head)->data;
		QueuePop(&obj->q1);
		return tmp;
	}
}

// 返回非空队列里的队尾元素
int myStackTop(MyStack* obj)
{
	if (QueueEmpty(&obj->q1))
		return QueueBack(&obj->q2);
	else
		return QueueBack(&obj->q1);
}

// 两个队列都为空时才为空
bool myStackEmpty(MyStack* obj)
{
	return QueueEmpty(&obj->q1) && QueueEmpty(&obj->q2);
}

// 先将两个队列申请的结点释放掉,再释放MyStack
void myStackFree(MyStack* obj)
{
	QueueDestroy(&obj->q1);
	QueueDestroy(&obj->q2);
	free(obj);
}