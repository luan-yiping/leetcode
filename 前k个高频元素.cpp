class Solution
{
public:
    class mycompare
    {
    public:
        bool operator()(const pair<int, int>& e1, const pair<int, int>& e2)
        {
            return e1.second > e2.second;
        }
    };
    vector<int> topKFrequent(vector<int>& nums, int k)
    {
        unordered_map<int, int> map;
        for (auto& e : nums) map[e]++;

        priority_queue<pair<int, int>, vector<pair<int, int>>, mycompare> q;
        for (auto& e : map)
        {
            q.push(e);
            if (q.size() > k) q.pop();
        }

        vector<int> ret(k);
        for (int i = 0; i < k; i++)
        {
            ret[i] = q.top().first;
            q.pop();
        }
        return ret;
    }
};