#include<bits/stdc++.h>
using namespace std;
int main()
{
    string a, b;
    cin >> a >> b;
    reverse(a.begin(), a.end());
    reverse(b.begin(), b.end());
    string c;
    int carry = 0;
    for (int i = 0; i < a.size() || i < b.size(); i++)
    {
        if (i < a.size()) carry += a[i] - '0';
        if (i < b.size()) carry += b[i] - '0';
        c.push_back(carry % 10 + '0');
        carry /= 10;
    }
    if (carry == 1)
        c.push_back('1');
    for (int i = c.size() - 1; i >= 0; i--)
    {
        printf("%d", c[i] - '0');
    }
    return 0;
}