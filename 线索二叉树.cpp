#include<iostream>
using namespace std;
#define Link	0	//表示该节点有非空孩子节点
#define Thread	1	//表示该节点有后续节点（对于右子树来说）
#define MAXSIZE  100

template<class T>
struct BT_Thread_Node
{
	T Data;
	BT_Thread_Node* Left_Child;
	BT_Thread_Node* Right_Child;
	int Ltag;
	int Rtag;
};

template<class T>
class Thread_Binary_tree
{
private:
	BT_Thread_Node<T>* Tree;
	BT_Thread_Node<T>* Pre_Node;
	BT_Thread_Node<T>* BT_Node_Stack[MAXSIZE];
	int Create_Thread_BTree(BT_Thread_Node<T>*& Tree);
	int Distory_Thread_BTree(BT_Thread_Node<T>*& Tree);
	void InOrder_Thread_Op(BT_Thread_Node<T>*& Tree);
	void _InOrder_Op(BT_Thread_Node<T>*& Tree);
public:
	Thread_Binary_tree();
	~Thread_Binary_tree();
	void InOrder_Thread();
	void _InOrder();
};

template<class T>
int Thread_Binary_tree<T>::Create_Thread_BTree(BT_Thread_Node<T>*& Tree)
{
	int Data;
	cin >> Data;
	if (Data == -1)
		Tree = NULL;
	else
	{
		Tree = new BT_Thread_Node<T>;
		Tree->Data = Data;
		Tree->Ltag = Link;
		Tree->Rtag = Link;
		Create_Thread_BTree(Tree->Left_Child);
		Create_Thread_BTree(Tree->Right_Child);
	}
	return 1;
}

template<class T>
int Thread_Binary_tree<T>::Distory_Thread_BTree(BT_Thread_Node<T>*& Tree)
{
	if (Tree == NULL)
		return 0;

	int Top = -1;
	BT_Thread_Node<T>* Cur_Node = Tree;
	while (Cur_Node)
	{
		while (Cur_Node->Ltag == Link)
		{
			Cur_Node = Cur_Node->Left_Child;
		}

		BT_Node_Stack[++Top] = Cur_Node;

		while (Cur_Node && Cur_Node->Rtag == Thread)
		{
			Cur_Node = Cur_Node->Right_Child;
			BT_Node_Stack[++Top] = Cur_Node;
		}

		Cur_Node = Cur_Node->Right_Child;
	}

	for (Top; Top != -1; Top--)
	{
		cout << BT_Node_Stack[Top]->Data;
		delete BT_Node_Stack[Top];
	}

	return 1;
}

template<class T>
void Thread_Binary_tree<T>::InOrder_Thread_Op(BT_Thread_Node<T>*& Tree)
{
	if (Tree == NULL)		//空返回上一节点
		return;

	InOrder_Thread_Op(Tree->Left_Child);		//左

	if (Tree->Left_Child == NULL)			//根
	{
		Tree->Ltag = Thread;
		Tree->Left_Child = Pre_Node;
	}
	if (Pre_Node != NULL && Pre_Node->Right_Child == NULL)
	{
		Pre_Node->Rtag = Thread;
		Pre_Node->Right_Child = Tree;
	}

	Pre_Node = Tree;
	InOrder_Thread_Op(Tree->Right_Child);		//右
}

template<class T>
void Thread_Binary_tree<T>::_InOrder_Op(BT_Thread_Node<T>*& Tree)
{
	if (Tree == NULL)
		return;

	BT_Thread_Node<T>* Cur_Node = Tree;

	while (Cur_Node)			//当前节点不能为空
	{
		while (Cur_Node->Ltag == Link)		//节点有左树时，寻找最左端的树
		{
			Cur_Node = Cur_Node->Left_Child;
		}
		cout << Cur_Node->Data << " ";

		while (Cur_Node && Cur_Node->Rtag == Thread)	//节点非空并且右树是线索树时查找最前的一个线索树节点
		{
			Cur_Node = Cur_Node->Right_Child;
			cout << Cur_Node->Data << " ";
		}


		Cur_Node = Cur_Node->Right_Child;		//右树不是线索树，查找该节点的右孩子节点
	}
	cout << endl;
}

template<class T>
Thread_Binary_tree<T>::Thread_Binary_tree() :Pre_Node(NULL)
{
	Create_Thread_BTree(Tree);
}

template<class T>
Thread_Binary_tree<T>::~Thread_Binary_tree()
{
	Distory_Thread_BTree(Tree);
}

template<class T>
void Thread_Binary_tree<T>::InOrder_Thread()
{
	InOrder_Thread_Op(Tree);
	Pre_Node = NULL;		//恢复值为空，便于下次线索化
}

template<class T>
void Thread_Binary_tree<T>::_InOrder()
{
	_InOrder_Op(Tree);
}