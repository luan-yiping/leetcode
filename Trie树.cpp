#include<iostream>
using namespace std;
const int N = 100010;
int son[N][26], cnt[N], idx;
void insert(string& str)
{
	int p = 0;
	for (int i = 0; str[i]; i++)
	{
		int u = str[i] - 'a';
		if (!son[p][u]) son[p][u] = ++idx;
		p = son[p][u];
	}
	cnt[p]++;
}
int query(string& str)
{
	int p = 0;
	for (int i = 0; str[i]; i++)
	{
		int u = str[i] - 'a';
		if (!son[p][u]) return 0;
		else p = son[p][u];
	}
	return cnt[p];
}
int main()
{
	int n;
	string x;
	scanf("%d", &n);
	while (n--)
	{
		char op;
		cin >> op;
		if (op == 'I')
		{
			cin >> x;
			insert(x);
		}
		// 'Q'
		else
		{
			cin >> x;
			printf("%d\n", query(x));
		}
	}
}