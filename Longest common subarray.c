// 二维dp
// dp[i][j]表示以A[i - 1],B[j - 1]作为结尾项的最长重复子数组的长度
int findLength(int* nums1, int nums1Size, int* nums2, int nums2Size)
{
    int** dp = (int**)malloc(sizeof(int*) * (nums1Size + 1));
    int i = 0, max = 0;
    for (i = 0; i <= nums1Size; i++)
    {
        dp[i] = (int*)calloc((nums2Size + 1), sizeof(int));
    }
    for (i = 1; i <= nums1Size; i++)
    {
        int j = 1;
        for (j = 1; j <= nums2Size; j++)
        {
            if (nums1[i - 1] == nums2[j - 1])
                dp[i][j] = dp[i - 1][j - 1] + 1;
            if (dp[i][j] > max)
                max = dp[i][j];
        }
    }
    return max;
}

// 滑动窗口
int FindMax(int* nums1, int i, int* nums2, int j, int len)
{
    int count = 0, max = 0;
    int k = 0;
    for (k = 0; k < len; k++)
    {
        if (nums1[i + k] == nums2[j + k])
        {
            count++;
        }
        else
        {
            max = fmax(count, max);
            count = 0;
        }
    }
    return fmax(count, max);
}
int _FindLength(int* nums1, int nums1Size, int* nums2, int nums2Size)
{
    int max = 0;
    // 未完全进入的时候
    for (int len = 1; len < nums1Size; len++)
    {
        max = fmax(max, FindMax(nums1, 0, nums2, nums2Size - len, len));
    }
    // 完全进入
    for (int len = nums2Size - nums1Size; len >= 0; len--)
    {
        max = fmax(max, FindMax(nums1, 0, nums2, len, nums1Size));
    }
    // 离开
    for (int len = 1; len < nums1Size; len++)
    {
        max = fmax(max, FindMax(nums1, len, nums2, 0, nums1Size - len));
    }
    return max;
}
int findLength(int* nums1, int nums1Size, int* nums2, int nums2Size)
{
    return nums1Size <= nums2Size ? _FindLength(nums1, nums1Size, nums2, nums2Size) : _FindLength(nums2, nums2Size, nums1, nums1Size);

}