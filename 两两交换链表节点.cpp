class Solution {
public:
    ListNode* swapPairs(ListNode* head)
    {
        if (!head) return nullptr;
        ListNode* dummy = new ListNode(-1, head);
        ListNode* c = head, * n = head->next, * p = dummy;
        while (c && n)
        {
            c->next = n->next;
            n->next = c;
            p->next = n;

            p = c;
            c = c->next;
            if (c) n = c->next;
        }
        return dummy->next;
    }
};