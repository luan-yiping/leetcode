class UnusualAdd
{
public:
    /*
        位的异或运算跟求'和'的结果一致：

        异或 1 ^ 1 = 0 1 ^ 0 = 1 0 ^ 0 = 0
        求和 1 + 1 = 0 1 + 0 = 1 0 + 0 = 0

        位的与运算跟求'进位‘的结果一致：

        位与 1 & 1 = 1 1 & 0 = 0 0 & 0 = 0
        进位 1 + 1 = 1 1 + 0 = 0 0 + 0 = 0
     */
    int addAB(int A, int B) 
   {
        int c, d;
        while (B != 0)
        {
            c = A ^ B;
            d = (A & B) << 1;
            A = c;
            B = d;
        }
        return c;
    }
};
