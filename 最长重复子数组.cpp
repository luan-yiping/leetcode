class Solution {
public:
    int findLength(vector<int>& nums1, vector<int>& nums2)
    {
        int ret = 0;
        vector<int> f(max(nums1.size(), nums2.size()) + 1, 0);
        for (int i = 1; i <= nums1.size(); i++)
        {
            for (int j = nums2.size(); j >= 1; j--)
            {
                if (nums1[i - 1] == nums2[j - 1]) f[j] = f[j - 1] + 1;
                else f[j] = 0;
                ret = max(ret, f[j]);
            }
            // for(auto& e : f) cout<<e<<" ";
            // cout<<endl;
        }
        return ret;
    }
};