class Solution {
public:
    unordered_map<int, int> map;
    // ��������
    vector<int> ret;
    TreeNode* dfs(vector<int>& preorder, int pl, int pr, vector<int>& inorder, int il, int ir)
    {
        if (pl > pr) return nullptr;
        auto root = new TreeNode(preorder[pl]);
        ret.push_back(root->val);
        int index = map[preorder[pl]];
        root->right = dfs(preorder, pl + index - il + 1, pr, inorder, index + 1, ir);
        root->left = dfs(preorder, pl + 1, pl + index - il, inorder, il, index - 1);
        return root;
    }
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder)
    {
        for (int i = 0; i < inorder.size(); i++) map.insert({ inorder[i],i });
        auto ans = dfs(preorder, 0, preorder.size() - 1, inorder, 0, inorder.size() - 1);
        reverse(ret.begin(), ret.end());
        for (auto& e : ret) cout << e << " ";
        cout << endl;
        return ans;
    }
};