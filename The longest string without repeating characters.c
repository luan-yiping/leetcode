int lengthOfLongestSubstring(char* s)
{
    int len = strlen(s);
    if (len == 0 || len == 1)
        return len;
    int index[256] = { 0 }, start = 0, max = 0, count = 0, i = 0;
    // index存储字符串各字符下标的下一位置,count为当前无重复子串的长度,max为最大的长度
    for (i = 0; i < len; i++)
    {
        // 出现重复字符
        if (index[s[i]] > start)
        {
            count = i - start;
            if (count > max)
                max = count;
            // 调整左边界的位置
            start = index[s[i]];
        }
        index[s[i]] = i + 1;
    }
    max = (max > i - start) ? max : i - start;
    return max;
}