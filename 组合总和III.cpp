class Solution
{
public:
    vector<vector<int>> ret;
    vector<int> path;
    int sum;
    void dfs(int k, int n, int start)
    {
        if (!k)
        {
            if (sum == n) ret.push_back(path);
            return;
        }
        for (int i = start; i <= 9; i++)
        {
            path.push_back(i);
            sum += i;
            dfs(k - 1, n, i + 1);
            path.pop_back();
            sum -= i;
        }
    }
    vector<vector<int>> combinationSum3(int k, int n)
    {
        dfs(k, n, 1);
        return ret;
    }
};