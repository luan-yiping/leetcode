void Swap(int* e1, int* e2)
{
    int tmp = *e1;
    *e1 = *e2;
    *e2 = tmp;
}

void Sort(int* left, int* right)
{
    while (left < right)
    {
        Swap(left, right);
        left++;
        right--;
    }
}
void nextPermutation(int* nums, int numsSize)
{
    int i = numsSize - 2;
    for (i = numsSize - 2; i >= 0; i--)
    {
        if (nums[i] < nums[i + 1])
            break;
    }
    if (i >= 0)
    {
        int j = numsSize - 1;
        for (j = numsSize - 1; j >= 0; j--)
        {
            if (nums[j] > nums[i])
                break;
        }
        Swap(&nums[i], &nums[j]);
    }
    Sort(&nums[i + 1], &nums[numsSize - 1]);

}