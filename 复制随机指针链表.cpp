class Solution {
public:
    Node* copyRandomList(Node* head)
    {
        for (auto p = head; p; p = p->next->next)
        {
            auto q = new Node(p->val);
            q->next = p->next;
            p->next = q;
        }

        for (auto p = head; p; p = p->next->next)
        {
            if (p->random)
                p->next->random = p->random->next;
        }

        auto dummy = new Node(-1), cur = dummy;
        dummy->next = head;

        for (auto p = head; p; p = p->next)
        {
            auto q = p->next;
            cur = cur->next = q;
            p->next = q->next;
        }

        return dummy->next;
    }
};







