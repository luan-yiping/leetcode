class Solution {
public:
    int longestConsecutive(vector<int>& nums)
    {
        unordered_set<int> hash;
        for (auto& x : nums) hash.insert(x);

        int ret = 0;
        for (auto& x : nums)
        {
            if (hash.count(x) && !hash.count(x - 1))
            {
                int y = x;
                while (hash.count(y + 1))
                {
                    y++;
                }
                ret = max(ret, y - x + 1);
            }
        }

        return ret;
    }
};