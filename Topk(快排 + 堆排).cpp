class Solution
{
public:
    // 快排
    int quick_sort(int l, int r, vector<int>& nums, int k)
    {
        if (l == r) return nums[k];
        int x = nums[l + r >> 1], i = l - 1, j = r + 1;
        while (i < j)
        {
            do i++; while (nums[i] > x);
            do j--; while (nums[j] < x);
            if (i < j)
                swap(nums[i], nums[j]);
        }
        if (k <= j) return quick_sort(l, j, nums, k);
        else return quick_sort(j + 1, r, nums, k);
    }
    int findKthLargest(vector<int>& nums, int k)
    {
        return quick_sort(0, nums.size() - 1, nums, k - 1);
    }

    // 堆排
    int findKthLargest(vector<int>& nums, int k)
    {
        priority_queue<int, vector<int>, greater<int>> pq;
        for (int i = 0; i < k; i++) pq.push(nums[i]);
        for (int i = k; i < nums.size(); i++)
        {
            if (nums[i] > pq.top())
            {
                pq.pop();
                pq.push(nums[i]);
            }
        }
        return pq.top();
    }
};