class Solution
{
public:
    void backtrack(vector<int>& candidates, vector<vector<int>>& res, vector<int>& path, int sum, int target, int startindex)
    {
        if (sum == target)
        {
            res.push_back(path)
            ;
            return;
        }
        
        for (int i = startindex; i < candidates.size() && sum + candidates[i] <= target; i++)
        
        {
            path.push_back(candidates[i]);
            sum += candidates[i];
            
            backtrack(candidates, res, path, sum, target, i);
            
            sum -= candidates[i];
            path.pop_back();
        }
    }
    vector<vector<int>> combinationSum(vector<int>& candidates, int target)
    {
        vector<vector<int>> res;
        vector<int> path;
        sort(candidates.begin(), candidates.end());
        backtrack(candidates, res, path, 0, target, 0);
        return res;
    }
};