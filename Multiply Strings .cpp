class Solution
{
public:
    // 字符串相加
    string addStrings(string num1, string num2)
    {
        if (num1.empty())
            return num2;
        if (num2.empty())
            return num1;
        string s;
        int i1 = num1.size() - 1, i2 = num2.size() - 1, carry = 0;
        while (i1 >= 0 || i2 >= 0)
        {
            int sum = 0;
            if (i1 < 0)
                sum = num2[i2] - '0' + carry;
            else if (i2 < 0)
                sum = num1[i1] - '0' + carry;
            else
                sum = num1[i1] - '0' + num2[i2] - '0' + carry;
            if (sum >= 10)
            {
                carry = 1;
                s.push_back(sum % 10 + '0');
            }
            else
            {
                carry = 0;
                s.push_back(sum + '0');
            }
            i1--;
            i2--;
        }
        if (carry == 1)
            s.push_back('1');
        reverse(s.begin(), s.end());
        return s;
    }
    // 将乘数的每一位和被乘数相乘(除了末尾位，其他需要补0)，分别将结果相加
    string multiply(string num1, string num2)
    {
        int i1 = num1.size() - 1, i2 = num2.size() - 1;
        if (num1[0] == '0' || num2[0] == '0')
            return "0";
        string ans;
        int count = 0;
        for (i2 = num2.size() - 1; i2 >= 0; i2--)
        {
            string tmp;
            int copy = count;
            // 补 0
            while (copy--)
                tmp.push_back('0');
            int carry = 0;
            for (i1 = num1.size() - 1; i1 >= 0; i1--)
            {
                int sum = (num1[i1] - '0') * (num2[i2] - '0') + carry;
                // 发生进位
                if (sum >= 10)
                {
                    tmp.push_back(sum % 10 + '0');
                    carry = sum / 10;
                }
                else
                {
                    tmp.push_back(sum + '0');
                    carry = 0;
                }
            }
            // 最后有进位
            if (carry != 0)
                tmp.push_back(carry + '0');
            // 逆置字符串
            reverse(tmp.begin(), tmp.end());
            // 将结果加到ans字符串上
            ans = addStrings(ans, tmp);
            // count控制补 0 的个数
            count++;
        }
        return ans;
    }

    // 优化
    // 乘数 num1 位数为 M ，被乘数 num2 位数为 N ， num1 x num2 结果 res 最大总位数为 M+N
//    num1[i] x num2[j] 的结果为 tmp(位数为两位，"0x","xy"的形式)，其第一位位于 res[i+j]，第二位位于 res[i+j+1]
    string multiply(string num1, string num2)
    {
        if (num1[0] == '0' || num2[0] == '0')
            return "0";
        int size1 = num1.size(), size2 = num2.size();
        string ans(size1 + size2, '0');
        for (int i = size2 - 1; i >= 0; i--)
        {
            for (int j = size1 - 1; j >= 0; j--)
            {
                int sum = (num1[j] - '0') * (num2[i] - '0') + ans[i + j + 1] - '0';
                ans[i + j + 1] = sum % 10 + '0';
                ans[i + j] = ans[i + j] - '0' + sum / 10 + '0';
            }
        }
        for (int i = 0; i < size1 + size2; i++)
        {
            if (ans[i] != '0')
                return ans.substr(i);
        }
        return "0";
    }
};