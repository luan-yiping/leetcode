class Solution {
public:
    int maxProduct(vector<int>& nums)
    {
        vector<int> v(nums.size(), 0);
        vector<int> g(nums.size(), 0);
        v[0] = nums[0];
        g[0] = nums[0];
        int ret = v[0];
        for (int i = 1; i < nums.size(); i++)
        {
            if (nums[i] < 0)
            {
                v[i] = max(nums[i], nums[i] * g[i - 1]);
                g[i] = min(nums[i], nums[i] * v[i - 1]);
            }
            else if (nums[i] > 0)
            {
                v[i] = max(nums[i], nums[i] * v[i - 1]);
                g[i] = min(nums[i], nums[i] * g[i - 1]);
            }
            ret = max(ret, v[i]);
        }
        return ret;
    }
};