#include<bits/stdc++.h>
using namespace std;
int main()
{
    string s1, s2;
    while (cin >> s1 >> s2)
    {
        // s1ͨ��� s2ƥ��
        int size1 = s1.size();
        int size2 = s2.size();
        vector<vector<bool>> v(size1 + 1, vector<bool>(size2 + 1, false));
        v[0][0] = true;
        for (int i = 1; i <= s1.size(); i++)
        {
            char ch = s1[i - 1];
            v[i][0] = v[i - 1][0] && (ch == '*');
            for (int j = 1; j <= s2.size(); j++)
            {
                char ch2 = s2[j - 1];
                if (ch == '*')
                    v[i][j] = v[i - 1][j] || v[i][j - 1];
                else
                    v[i][j] = v[i - 1][j - 1] && (ch == '?' || tolower(ch) == tolower(ch2));
            }
        }
        if (v[size1][size2] == true)
            cout << "true" << endl;
        else
            cout << "false" << endl;
    }
}