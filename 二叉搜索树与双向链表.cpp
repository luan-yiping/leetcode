class Solution
{
public:
    // pre 为 cur 的前一结点,head指向链表第一个结点
    Node* pre, * head;
    // 二叉搜索树中序遍历为升序
    void dfs(Node* cur)
    {
        if (cur == NULL)
            return;
        // 递归左子树
        dfs(cur->left);
        // 处理根结点
        if (pre != NULL)
            pre->right = cur;
        else
            head = cur;
        cur->left = pre;
        pre = cur;
        // 递归右子树
        dfs(cur->right);
    }
    Node* treeToDoublyList(Node* root)
    {
        if (root == NULL)
            return NULL;
        dfs(root);
        // 构造循环链表
        head->left = pre;
        pre->right = head;
        return head;
    }
};