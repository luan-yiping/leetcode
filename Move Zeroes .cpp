class Solution
{
public:
    void moveZeroes(vector<int>& nums)
    {
        int prev = 0, cur = 0;
        
        while (cur < nums.size())
        {
            if (nums[cur] == 0)
            {
                ++cur;
            }
            else
            {
                if (cur > prev)
                    swap(nums[prev], nums[cur]);
                ++prev;
                ++cur;
            }
        }
    }
};