class Solution
{
public:
    int res = 0;
    void dfs(TreeNode* root, int& k)
    {
        if (root == nullptr)
            return;
        dfs(root->right, k);
        if (--k == 0)
        {
            res = root->val;
            return;
        }
        dfs(root->left, k);
    }
    int kthLargest(TreeNode* root, int k)
    {
        dfs(root, k);
        return res;
    }
};