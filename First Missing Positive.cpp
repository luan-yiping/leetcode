class Solution
{
public:

    int firstMissingPositive(vector<int>& nums)
    {
        int size = nums.size();
        // 原地哈希，将 1，2，3，.......分别放到下标为 n - 1 的位置处，再次遍历数组如果 i + 1 ！= nums[i]，则缺失的是 i + 1，若都相等，则缺失的是 size + 1
        for (int i = 0; i < size; i++)
        {
            // 将正整数分别放到下标 n - 1的 位置
            while (nums[i] >= 1 && nums[i] <= size && nums[i] != nums[nums[i] - 1])
                swap(nums[i], nums[nums[i] - 1]);
        }
        for (int i = 0; i < size; i++)
        
        {
            if (i + 1 != nums[i])
                return i + 1;
        }
        return size + 1;
    }
};

