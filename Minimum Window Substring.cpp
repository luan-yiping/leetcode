class Solution
{
public:
    string minWindow(string s, string t)
    {
        // 初始化为128个值为0的元素，记录t字符串中字符出现次数
        vector<int> map(128);
        for (auto& ch : t)
            map[ch]++;
        // count为缺少几个字符可以覆盖t中所有的字符
        int size = s.size(), count = t.size(), left = 0, right = 0;
        int minStart = 0, minLen = INT_MAX;
        while (right < size)
        {
            if (map[s[right]] > 0)
                count--;
                
            // 使未出现在t中的字符数量为负数，出现在t中的字符数量减1
            
            map[s[right]]--;
            right++;
            // 字符被完全涵盖
            while (count == 0)
            {
                if (right - left < minLen)
                {
                    minStart = left;
                    minLen = right - left;
                }
                // 若s[left]为t中字符，则count++ 
                if (++map[s[left]] > 0)
                    count++;
                left++;
            }
        }
        if (minLen == INT_MAX)
            return "";
        else
            return s.substr(minStart, minLen);
    }
};