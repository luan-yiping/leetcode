#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;

const int N = 1010;
int f[N];
int v[N], w[N];
int n, vt;

int main()
{
    cin >> n >> vt;
    for (int i = 1; i <= n; i++) cin >> v[i] >> w[i];
    for (int i = 1; i <= n; i++)
    {
        for (int j = vt; j >= 1; j--)
        {
            f[j] = f[j];
            if (j >= v[i]) f[j] = max(f[j], f[j - v[i]] + w[i]);
        }
    }
    cout << f[vt] << endl;
}
