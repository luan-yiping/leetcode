#include<iostream>
#include<algorithm>
using namespace std;
template<class K>
struct BSTreeNode
{
	BSTreeNode(const K& val)
		:_left(nullptr)
		,_right(nullptr)
		,_val(val)
	{}
	BSTreeNode* _left;
	BSTreeNode* _right;
	K _val;
};
template<class K>
class BSTree
{
	typedef BSTreeNode<K> Node;
public:
	BSTree()
		:_root(nullptr)
	{}
	BSTree(const BSTree<T>& t)
	{
		Copy(t._root);
	}
	// t1 = t2
	BSTree& operator=(BSTree<T> t)
	{
		swap(_root,t._root);
		return *this;
	}
	Node* Copy(Node* root)
	{
		if (root == nullptr) return nullptr;
		Node* copy = new Node(root->_val);
		copy->_left = Copy(root->_left);
		copy->_right = Copy(root->_right);
		return copy;
	}
	~BSTree()
	{
		Destory();
		_root = nullptr;
	}
	void Destory()
	{
		_Destory(_root);
	}
	void _Destory(Node* root)
	{
		if (root == nullptr) return;
		_Destory(root->_left);
		_Destory(root->_right);
		delete root;
	}
	bool _InsertR(Node*& root,const K& val)
	{
		if (root == nullptr)
		{
			root = new Node(val);
			return true;
		}
		if (val < root->_val) return _InsertR(root->_left, val);
		else if (val > root->_val) return _InsertR(root->_right, val);
		else return false;
	}
	bool InsertR(const K& val)
	{
		return _InsertR(_root,val);
	}
	bool _EraseR(Node*& root,const K& val)
	{
		if (root == nullptr) return false;
		if (val < root->_val) _EraseR(root->_left, val);
		else if (val > root->_val) _EraseR(root->_right,val);
		else
		{
			if (root->_left == nullptr || root->_right == nullptr)
			{
				Node* tmp = root;
				root = root->_left != nullptr ? root->_left : root->_right;
				delete tmp;
			}
			// 两个孩子
			else
			{
				Node* cur = root->_left;
				while (cur->_right) cur = cur->_right;
				root->_val = cur->_val;
				_EraseR(root->_left,cur->_val);
			}
			return true;
		}
	}
	bool EraseR(const K& val)
	{
		return _EraseR(_root,val);
	}
	Node* _FindR(Node* root,const K& val)
	{
		if (root == nullptr)
			return nullptr;
		if (val < root->_val) return _FindR(root->_left, val);
		else if (val > root->_val) return _FindR(root->_right, val);
		else return root;
	}
	Node* FindR(const K& val)
	{
		return _FindR(_root,val);
	}
	bool Insert(const K& val)
	{
		if (_root == nullptr)
		{
			_root = new Node(val);
			return true;
		}
		Node* p = new Node(val);
		Node* cur = _root, * parent = cur;
		while (cur)
		{
			if (val < cur->_val)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (val > cur->_val)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				return false;
			}
		}
		if (val < parent->_val) parent->_left = p;
		else parent->_right = p;
		return true;
	}
	void _InOrder(Node* root)
	{
		if (root == nullptr)
			return;
		_InOrder(root->_left);
		cout << root->_val<<" ";
		_InOrder(root->_right);
	}
	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}
	Node* Find(const K& val)
	{
		Node* cur = _root;
		while (cur)
		{
			if (val < cur->_val) cur = cur->_left;
			else if (val > cur->_val) cur = cur->_right;
			else return cur;
		}
		return nullptr;
	}
	bool Erase(const K& val)
	{
		Node* cur = _root, *parent = cur;
		while (cur)
		{
			if (val < cur->_val)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (val > cur->_val)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				//// 叶子节点
				//if (cur->_left == nullptr && cur->_right == nullptr)
				//{
				//	if (cur->_val > parent->_val) parent->_right = nullptr;
				//	else parent->_left = nullptr;
				//}
				//// 一个孩子
				//else if (cur->_left == nullptr || cur->_right == nullptr)
				//{
				//	if (cur->_val > parent->_val) parent->_right = (cur->_left != nullptr) ? cur->_left : cur->_right;
				//	else parent->_left = (cur->_left != nullptr) ? cur->_left : cur->_right;
				//}

				// 叶子节点可以合并到一个孩子
				if (cur->_left == nullptr || cur->_right == nullptr)
				{
					if (cur->_val > parent->_val) parent->_right = (cur->_left != nullptr) ? cur->_left : cur->_right;
					else parent->_left = (cur->_left != nullptr) ? cur->_left : cur->_right;
					delete cur;
				}
				// 两个孩子
				else
				{
					Node* p = parent, * c = cur->_left;
					while (c->_right)
					{
						p = c;
						c = c->_right;
					}
					cur->_val = c->_val;
					if (c->_val > p->_val) p->_right = c->_left;
					else p->_left = c->_left;
					delete c;
				}
				return true;
			}
		}
		return false;
	}
private:
	Node* _root;
};
