#include"BinarySearchTree KV.h"

void test1()
{
	BSTree<string, string> dict;
	dict.Insert("sort","排序");
	dict.Insert("Find", "查找");
	dict.Insert("left", "左边，剩余");
	dict.Insert("right", "右边");
	dict.Insert("Student", "学生");

	string s = "sort";
	BSTreeNode<string, string>* ret = dict.Find(s);
	if (ret == nullptr)
	{
		cout << "无此单词 : " << s << endl;
	}
	else
	{
		cout << s << ":" << ret->_val << endl;
	}
}
void test2()
{
	string arr[] = {"sort","sort","left","left","right","left","right","left","sort","sort","left","right","sort"};
	BSTree<string, int> count;
	for (auto& e : arr)
	{
		auto ret = count.Find(e);
		if (ret == nullptr)
		{
			count.Insert(e,1);
		}
		else
		{
			ret->_val++;
		}
	}
	count.InOrder();
}
int main()
{
	// test1();
	// test2();
}