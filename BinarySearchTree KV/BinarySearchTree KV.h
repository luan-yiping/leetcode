#include<iostream>
#include<algorithm>
using namespace std;
template<class K,class V>
struct BSTreeNode
{
	BSTreeNode(const K& key,const V& val)
		:_left(nullptr)
		, _right(nullptr)
		, _key(key)
		,_val(val)
	{}
	BSTreeNode* _left;
	BSTreeNode* _right;
	K _key;
	V _val;
};
template<class K,class V>
class BSTree
{
	typedef BSTreeNode<K,V> Node;
public:
	BSTree()
		:_root(nullptr)
	{}
	BSTree(const BSTree<K,V>& t)
	{
		Copy(t._root);
	}
	// t1 = t2
	BSTree<K,V>& operator=(BSTree<K,V> t)
	{
		swap(_root, t._root);
		return *this;
	}
	Node* Copy(Node* root)
	{
		if (root == nullptr) return nullptr;
		Node* copy = new Node(root->_key);
		copy->_left = Copy(root->_left);
		copy->_right = Copy(root->_right);
		return copy;
	}
	~BSTree()
	{
		Destory();
		_root = nullptr;
	}
	void Destory()
	{
		_Destory(_root);
	}
	void _Destory(Node* root)
	{
		if (root == nullptr) return;
		_Destory(root->_left);
		_Destory(root->_right);
		delete root;
	}
	bool Insert(const K& key,const V& val)
	{
		if (_root == nullptr)
		{
			_root = new Node(key,val);
			return true;
		}
		Node* p = new Node(key,val);
		Node* cur = _root, * parent = cur;
		while (cur)
		{
			if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				return false;
			}
		}
		if (key < parent->_key) parent->_left = p;
		else parent->_right = p;
		return true;
	}
	void _InOrder(Node* root)
	{
		if (root == nullptr)
			return;
		_InOrder(root->_left);
		cout << root->_key << " " << root->_val<<endl;
		_InOrder(root->_right);
	}
	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}
	Node* Find(const K& key)
	{
		Node* cur = _root;
		while (cur)
		{
			if (key < cur->_key) cur = cur->_left;
			else if (key > cur->_key) cur = cur->_right;
			else return cur;
		}
		return nullptr;
	}
	bool Erase(const K& key)
	{
		Node* cur = _root, * parent = cur;
		while (cur)
		{
			if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				//// 叶子节点
				//if (cur->_left == nullptr && cur->_right == nullptr)
				//{
				//	if (cur->_key > parent->_key) parent->_right = nullptr;
				//	else parent->_left = nullptr;
				//}
				//// 一个孩子
				//else if (cur->_left == nullptr || cur->_right == nullptr)
				//{
				//	if (cur->_key > parent->_key) parent->_right = (cur->_left != nullptr) ? cur->_left : cur->_right;
				//	else parent->_left = (cur->_left != nullptr) ? cur->_left : cur->_right;
				//}

				// 叶子节点可以合并到一个孩子
				if (cur->_left == nullptr || cur->_right == nullptr)
				{
					if (cur->_key > parent->_key) parent->_right = (cur->_left != nullptr) ? cur->_left : cur->_right;
					else parent->_left = (cur->_left != nullptr) ? cur->_left : cur->_right;
					delete cur;
				}
				// 两个孩子
				else
				{
					Node* p = parent, * c = cur->_left;
					while (c->_right)
					{
						p = c;
						c = c->_right;
					}
					cur->_key = c->_key;
					if (c->_key > p->_key) p->_right = c->_left;
					else p->_left = c->_left;
					delete c;
				}
				return true;
			}
		}
		return false;
	}
private:
	Node* _root;
};
