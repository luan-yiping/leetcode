#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
int n, m;
const int N = 200010;
int p[N];
struct Edge
{
    int a, b, w;
    bool operator<(const Edge& E)
    {
        return w < E.w;
    }
}edges[N];
int find(int x)
{
    if (p[x] != x) p[x] = find(p[x]);
    return p[x];
}
int main()
{
    cin >> n >> m;
    for (int i = 0; i < m; i++)
    {
        int u, v, w;
        cin >> u >> v >> w;
        edges[i] = { u,v,w };
    }
    sort(edges, edges + m);
    for (int i = 1; i <= n; i++) p[i] = i;
    int res = 0, cnt = 0;
    for (int i = 0; i < m; i++)
    {
        int a = edges[i].a, b = edges[i].b, w = edges[i].w;
        a = find(a), b = find(b);
        if (a != b)
        {
            p[a] = b;
            res += w;
            cnt++;
        }
    }
    if (cnt != n - 1) cout << "impossible" << endl;
    else cout << res << endl;

}