int min(int x, int y, int z)
{
    int min = (x < y) ? x : y;
    return (min < z) ? min : z;
}
int nthUglyNumber(int n)
{
    int* ret = (int*)malloc(sizeof(int) * n);
    ret[0] = 1;
    int a = 0, b = 0, c = 0;
    // a ��2   b ��3  c ��5
    int i = 1;
    for (i = 1; i < n; i++)
    {
        ret[i] = min(ret[a] * 2, ret[b] * 3, ret[c] * 5);
        if (ret[i] == ret[a] * 2)
            a++;
        if (ret[i] == ret[b] * 3)
            b++;
        if (ret[i] == ret[c] * 5)
            c++;
    }
    return ret[n - 1];
}