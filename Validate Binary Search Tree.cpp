class Solution
{
public:

    // �ݹ�
    long prev = LONG_MIN;
    bool isValidBST(TreeNode* root)
    {
        if (root == nullptr)
            return true;
        if (!isValidBST(root->left))
            return false;
        if (root->val <= prev)
            return false;
        prev = root->val;
        return isValidBST(root->right);
    }

    // ����
    long prev = LONG_MIN;
    bool isValidBST(TreeNode* root)
    {
        stack<TreeNode*> st;
        if (root != nullptr)
            st.push(root);
        while (!st.empty())
        {
            TreeNode* node = st.top();
            if (node != nullptr)
            {
                st.pop();
                if (node->right)
                    st.push(node->right);
                st.push(node);
                st.push(nullptr);
                if (node->left)
                    st.push(node->left);
            }
            else
            {
                st.pop();
                node = st.top();
                if (node->val <= prev)
                    return false;
                prev = node->val;
                st.pop();
            }
        }
        return true;
    }
};