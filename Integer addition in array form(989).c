void reverse(int* start, int* end)
{
    while (start < end)
    {
        int tmp = *start;
        *start = *end;
        *end = tmp;
        start++;
        end--;
    }
}

int* addToArrayForm(int* A, int ASize, int K, int* returnSize)
{
    int ksize = 0;
    int Kcopy = K;
    while (Kcopy)
    {
        ksize++;
        Kcopy /= 10;
    }
    int len = (ASize > ksize) ? ASize : ksize;
    int* arr = (int*)malloc(sizeof(int) * (len + 1));
    int carry = 0, j = ASize - 1, i = 0, count = 0, tmp = 0;
    while (j >= 0 || K > 0)
    {
        if (j >= 0)
        {
            tmp = A[j] + K % 10 + carry;
        }
        else
        {
            tmp = K % 10 + carry;
        }
        carry = 0;
        if (tmp >= 10)
        {
            carry += 1;
            arr[i] = tmp % 10;
        }
        else
        {
            arr[i] = tmp;
        }
        i++;
        j--;
        count++;
        tmp = 0;
        K /= 10;
    }
    if (carry == 1)
    {
        arr[i] = carry;
        count++;
    }
    reverse(arr, arr + count - 1);
    *returnSize = count;
    return arr;
}