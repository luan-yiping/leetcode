class LRUCache
{
public:

    struct Node
    {
        Node(int _key, int _val)
            :key(_key)
            , val(_val)
            , left(nullptr)
            , right(nullptr)
        {}
        int key;
        int val;
        Node* left;
        Node* right;
    }*L, * R;
    unordered_map<int, Node*> hash;
    int n;

    LRUCache(int capacity)
    {
        n = capacity;
        L = new Node(-1, -1);
        R = new Node(-1, -1);
        L->right = R;
        R->left = L;
    }

    int get(int key)
    {
        if (hash.count(key) == 0) return -1;
        else
        {
            Node* p = hash[key];
            remove(p);
            insert(p);
            return p->val;
        }
    }

    void put(int key, int value)
    {
        if (hash.count(key))
        {
            Node* p = hash[key];
            p->val = value;
            remove(p);
            insert(p);
        }
        // 
        else
        {
            if (hash.size() == n)
            {
                Node* p = R->left;
                remove(p);
                hash.erase(p->key);
                delete p;
            }
            Node* p = new Node(key, value);
            hash[key] = p;
            insert(p);
        }
    }
    void remove(Node* p)
    {
        p->left->right = p->right;
        p->right->left = p->left;
    }
    void insert(Node* p)
    {
        p->right = L->right;
        L->right->left = p;
        p->left = L;
        L->right = p;
    }
};

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache* obj = new LRUCache(capacity);
 * int param_1 = obj->get(key);
 * obj->put(key,value);
 */