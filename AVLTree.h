#pragma once
#include<iostream>
using namespace std;
template<class K,class V>

struct AVLTreeNode
{
	AVLTreeNode(const pair<K,V>& kv)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, bf(0)
		,_kv(kv)
	{}
	AVLTreeNode<K,V>* _left;
	AVLTreeNode<K,V>* _right;
	AVLTreeNode<K,V>* _parent;
	int _bf; // 平衡因子，右子树高度-左子树高度
	pair<K, V> _kv;
};
template<class K,class V>
class AVLTree
{
	typedef AVLTreeNode<K, V> Node;
public:
	AVLTree()
		:_root(nullptr)
	{}
	bool Insert(const pair<K,V>& kv)
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return true;
		}
		Node* parent = _root, * cur = _root;
		Node* p = new Node(kv);
		while (cur)
		{
			if (kv.fisrt < cur->_kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (kv.first > cur->_kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				return false;
			}
		}
		p->_parent = parent;
		if (kv.first < parent->_kv.first) parent->_left = p;
		else parent->_right = p;

		// 更新平衡因子并旋转
		while (cur != _root)
		{
			if (parent->_right == cur) ++parent->_bf;
			else --parent->_bf;

			if (parent->_bf == 0) break;
			else if (parent->_bf == 1 || parent->_bf == -1)
			{
				cur = parent;
				parent = cur->_parent;
			}
			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				if (parent->_bf == -2)
				{
					if (cur->_bf == -1) RotateR(parent);
					else if (cur->_bf == 1) RotateLR(parent);
				}
				// parent->_bf == 2
				else
				{
					if (cur->_bf == 1) RotateL(parent);
					else if (cur->_bf == -1) RotateRL(parent);
				}
				break;
			}
		}
	}
	void RotateLR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		int bf = subLR->_bf;

		RotateL(subL);
		RotateR(parent);

		if (bf == 1)
		{
			subLR->_bf = 0;
			subL->_bf = -1;
			parent->_bf = 0;
		}
		/*else if (bf == -1)
		{
			subLR->_bf = 0;
			subL->_bf = 0;
			parent->_bf = 0;
		}
		else
		{
			subLR->_bf = 0;
			subL->_bf = 0;
			parent->_bf = 0;
		}*/
		else
		{
			subLR->_bf = 0;
			subL->_bf = 0;
			parent->_bf = 0;
		}
	}
	void RotateRL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		int bf = subRL->_bf;

		RotateR(subR);
		RotateL(parent);

		if (bf == 1)
		{
			subRL->_bf = 0;
			subR->_bf = 0;
			parent->_bf = -1;
		}
		else if (bf == -1)
		{
			subRL->_bf = 0;
			subR->_bf = 1;
			parent->_bf = 0;
		}
		// bf == 0
		else
		{
			subRL->_bf = 0;
			subR->_bf = 0;
			parent->_bf = 0;
		}
	}
	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		Node* parentParent = parent->_parent;
		parent->_right = subRL;
		subR->_left = parent;
		if (subRL)
			subRl->_parent = parent;
		if (parent == _root)
		{
			_root = subR;
			_root->_parent = nullptr;
		}
		else
		{
			if (parentParent->_left == parent) parentParent->_left = subR;
			else parentParent->_right = subR;

			subR->_parent = parentParent;
			parentParent = subR;
		}
		subR->_bf = parent->_bf = 0;
	}
	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		Node* parentParent = parent->_parent;
		parent->_left = subLR;
		subL->_right = parent;
		if(subLR)
			subLR->_parent = parent;
		if (parent == _root)
		{
			_root = subL;
			_root->_parent = nullptr;
		}
		else
		{
			if (parentParent->_left == parent) parentParent->_left = subL;
			else parentParent->_right = subL;
			subL->_parent = parentParent;
			parentParent = subL;
		}
		subL->_bf = parent->_bf = 0;
	}
	void RotateL(Node* parent)
	{

	}
private:
	Node* _root;
};