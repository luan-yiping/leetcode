// 构建辅助栈来模拟入栈和出栈的过程,若最后栈中数据个数为0，则说明顺序正确
bool validateStackSequences(int* pushed, int pushedSize, int* popped, int poppedSize)
{
    int* stack = (int*)malloc(sizeof(int) * pushedSize);
    int top = 0, pushi = 0, popi = 0;
    while (pushi < pushedSize)
    {
        stack[top] = pushed[pushi];
        top++;
        pushi++;
        while (top && stack[top - 1] == popped[popi])
        {
            --top;
            popi++;
        }
    }
    
    return !top;
}
