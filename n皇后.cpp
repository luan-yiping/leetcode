#include<iostream>
using namespace std;
const int N = 20;
char g[N][N];
int a[N];
int n, cnt;

bool check(int x, int y)
{
    for (int i = 1; i <= x; i++)
    {
        if (a[i] == y) return false;
        if (i + a[i] == x + y) return false;
        if (i - a[i] == x - y) return false;
    }
    return true;
}
void dfs(int row)
{
    if (row == n + 1)
    {
        cnt++;
        for (int i = 1; i <= n; i++)
        {
            g[i][a[i]] = 'Q';
        }
        for (int i = 1; i <= n; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                printf("%c", g[i][j]);
            }
            printf("\n");
        }
        printf("\n");
    }
    for (int i = 1; i <= n; i++)
    {
        if (check(row, i))
        {
            a[row] = i;
            dfs(row + 1);
            a[row] = 0;
            g[row][i] = '.';
        }
    }
}
int main()
{
    scanf_s("%d", &n);
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= n; j++)
        {
            g[i][j] = '.';
        }
    }
    dfs(1);
    printf("%d\n", cnt);
    return 0;
}