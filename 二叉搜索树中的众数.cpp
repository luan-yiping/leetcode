class Solution
{
private:
    int maxCount;
    int count;
    TreeNode* pre;
    vector<int> ret;
public:
    void searchBST(TreeNode* root)
    {
        if (root == nullptr) return;
        searchBST(root->left);

        if (!pre) count = 1;
        else if (pre->val == root->val) count++;
        else count = 1;
        pre = root;

        if (count == maxCount) ret.push_back(root->val);
        if (count > maxCount)
        {
            maxCount = count;
            ret.clear();
            ret.push_back(root->val);
        }
        searchBST(root->right);
    }
    vector<int> findMode(TreeNode* root)
    {
        searchBST(root);
        return ret;
    }
};
