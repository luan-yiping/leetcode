class Solution
{
public:
    void backtrack(int n, int left, int right, vector<string>& ans, string& path)
    {
        if (right > left)
            return;
        if (left == n && right == n)
        {
            ans.push_back(path);
            return;
        }
        if (left < n)
        {
            path.push_back('(');
            backtrack(n, left + 1, right, ans, path);
            path.pop_back();
        }
        if (right < n)
        {
            path.push_back(')');
            backtrack(n, left, right + 1, ans, path);
            path.pop_back();
        }
    }
    vector<string> generateParenthesis(int n)
    {
        vector<string> ans;
        
        string path;
        path.push_back('(');
        backtrack(n, 1, 0, ans, path);
        return ans;
    }
};