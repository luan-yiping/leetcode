#include<iostream>
#include<algorithm>
using namespace std;
const int N = 100010;
int m, y, result;
typedef pair<int, int> pii;
pii q[N];
int s[2][N];
int main()
{
    scanf_s("%d", &m);
    for (int i = 1; i <= m; i++) scanf_s("%d%d", &q[i].first, &q[i].second);
    sort(q + 1, q + m + 1);
    for (int i = 0; i < 2; i++)
    {
        for (int j = 1; j <= m; j++)
            s[i][j] = s[i][j - 1] + (q[j].second == i);
    }
    int cnt = 0, res = 0;
    for (int i = 1; i <= m; i++)
    {
        if (i > 1 && q[i].first == q[i - 1].first)
            continue;
        int t = s[0][i - 1] + s[1][m] - s[1][i - 1];
        if (t >= cnt)
        {
            cnt = t;
            res = q[i].first;
        }
    }
    printf("%d", res);
}