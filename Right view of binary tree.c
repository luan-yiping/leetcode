#define MAX 10000
typedef struct TreeNode TreeNode;
typedef TreeNode* QDataType;
typedef struct QueueNode
{
	QDataType data;
	struct QueueNode* next;
}QueueNode;
typedef struct Queue
{
	QueueNode* head;
	QueueNode* tail;
}Queue;
// 判空
bool QueueEmpty(Queue* pq)
{
	assert(pq);
	return pq->head == NULL;
}

void QueueInit(Queue* pq)
{
	assert(pq);
	pq->head = pq->tail = NULL;
}
// 入队
void QueuePush(Queue* pq, QDataType x)
{
	assert(pq);
	QueueNode* newNode = (QueueNode*)malloc(sizeof(QueueNode));
	if (newNode == NULL)
	{
		printf("malloc failed\n");
		exit(-1);
	}
	newNode->data = x;
	newNode->next = NULL;
	if (pq->head == NULL)
	{
		pq->head = pq->tail = newNode;
	}
	else
	{
		pq->tail->next = newNode;
		pq->tail = pq->tail->next;
	}
}
// 出队
void QueuePop(Queue* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));
	QueueNode* next = pq->head->next;
	free(pq->head);
	pq->head = next;
}
// 获取队尾元素
QDataType QueueBack(Queue* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));
	return pq->tail->data;
}
// 获取队头元素
QDataType QueueFront(Queue* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));
	return pq->head->data;
}
// 销毁队列
void QueueDestroy(Queue* pq)
{
	assert(pq);
	QueueNode* cur = pq->head;
	while (cur)
	{
		QueueNode* next = cur->next;
		free(cur);
		cur = next;
	}
}
// 队列元素个数
int QueueSize(Queue* pq)
{
	assert(pq);
	int count = 0;
	QueueNode* cur = pq->head;
	while (cur)
	{
		count++;
		cur = cur->next;
	}
	return count;
}
int* rightSideView(struct TreeNode* root, int* returnSize)
{
	int* ret = (int*)malloc(sizeof(int) * MAX);
	Queue q;
	QueueInit(&q);
	*returnSize = 0;
	if (root == NULL)
		return NULL;
	QueuePush(&q, root);
	while (!QueueEmpty(&q))
	{
		int size = QueueSize(&q);
		ret[(*returnSize)++] = QueueFront(&q)->val;
		while (size--)
		{
			TreeNode* tmp = QueueFront(&q);
			QueuePop(&q);
			if (tmp->right != NULL)
			{
				QueuePush(&q, tmp->right);
			}
			if (tmp->left != NULL)
			{
				QueuePush(&q, tmp->left);
			}
		}
	}
	return ret;
}