#define Maxsize 1500

int** levelOrder(struct TreeNode* root, int* returnSize, int** returnColumnSizes)
{
    *returnSize = 0;
    if (root == NULL)
        return NULL;
    int** result = (int**)malloc(sizeof(int*) * Maxsize);       //为返回的二维数组申请空间
    int i = 0, j = 0, k = 0, flag = 0;
    struct TreeNode* Myqueue[Maxsize];   //存储结点的队列，其中的元素类型为struct TreeNode*  
    *returnColumnSizes = (int*)malloc(sizeof(int) * Maxsize);    //为记录每层元素个数的数组分配空间
    Myqueue[j++] = root;                   //首先将根节点入队
    while (i != j)
    {
        flag = j;         //用flag标记j的位置，因为后续的进队操作会改变j的值
        k = 0;
        result[*returnSize] = (int*)malloc(sizeof(int) * (flag - i));  //为二维数组的每一行申请空间
        while (i < flag)
        {
            struct TreeNode* p = Myqueue[i];       //出队，将节点值存入返回数组
            result[*returnSize][k++] = p->val;
            if (p->left)  
                                    //如果有左孩子则左孩子入队
                Myqueue[j++] = p->left;
            if (p->right)                         //如果有左孩子则左孩子入队
                Myqueue[j++] = p->right;
            i++;
        }
        (*returnColumnSizes)[*returnSize] = k;     //记录本层的节点个数
        (*returnSize)++;                         //下一层
    }
    return result;
}