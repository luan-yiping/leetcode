class Solution
{
public:
    // ݹ
    int dfs(TreeNode* root, int i)
    {
        if (root == nullptr)
            return 0;
        int tmp = i * 10 + root->val;
        if (root->left == nullptr && root->right == nullptr)
            return tmp;
        return dfs(root->left, tmp) + dfs(root->right, tmp);
    }
    int sumNumbers(TreeNode* root)
    {
        return dfs(root, 0);
    }
    // 
    int sumNumbers(TreeNode* root)
    {
        if (root == nullptr)
            return 0;
        int ans = 0;
        queue<TreeNode*>q;
        q.push(root);
        while (!q.empty())
        {
            TreeNode* tmp = q.front();
            q.pop();
            if (tmp->left == nullptr && tmp->right == nullptr)
                ans += tmp->val;
            if (tmp->left)
            {
                tmp->left->val += tmp->val * 10;
                q.push(tmp->left);
            }
            if (tmp->right)
            {
                tmp->right->val += tmp->val * 10;
                q.push(tmp->right);
            }
        }
        return ans;
    }
};