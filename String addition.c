#include<stdio.h>
#include<stdlib.h>
#include<string.h>
void reverse(char* ret)
{
    int len = strlen(ret);
    char* start = ret, * end = ret + len - 1;
    while (start < end)
    {
        char tmp = *start;
        *start = *end;
        *end = tmp;
        start++;
        end--;
    }
}
// 模拟进位加法
char* addStrings(char* num1, char* num2)
{
    int len1 = strlen(num1);
    int len2 = strlen(num2);
    char* tail1 = num1 + len1 - 1, * tail2 = num2 + len2 - 1;
    int sum = 0, carry = 0, count = 0;
    int size = (len1 > len2) ? len1 + 2 : len2 + 2;
    char* ret = (char*)calloc(size, sizeof(char));
    while (tail1 >= num1 || tail2 >= num2)
    {
        if (tail1 < num1)
            sum += (*tail2 - '0') + carry;
        else if (tail2 < num2)
            sum += (*tail1 - '0') + carry;
        else
            sum += (*tail1 - '0') + (*tail2 - '0') + carry;
        if (sum >= 10)
        {
            ret[count++] = (sum % 10) + '0';
            carry = 1;
        }
        else
        {
            ret[count++] = sum + '0';
            carry = 0;
        }
        tail1--;
        tail2--;
        sum = 0;
    }
    if (carry == 1)
        ret[count] = 1 +'0';
    reverse(ret);
    return ret;
}
int main()
{
    char num1[] = "1";
    char num2[] = "9";
    printf("%s\n",addStrings(num1, num2));
}