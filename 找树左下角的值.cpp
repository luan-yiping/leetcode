class Solution
{
public:
    int findBottomLeftValue(TreeNode* root)
    {
        queue<TreeNode*> q;
        q.push(root);
        int ret = 0;
        while (q.size())
        {
            int n = q.size();
            for (int i = 0; i < n; i++)
            {
                TreeNode* node = q.front();
                q.pop();
                if (i == 0) ret = node->val;
                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }
        }
        return ret;
    }
};