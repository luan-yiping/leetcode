int add(int a, int b)
{
    while (b != 0) // 判断是否还有进位
    {
        int plus = (a ^ b); // 计算无进位的结果
        b = ((unsigned int)(a & b) << 1); // 计算进位的结果  
        
        a = plus; 
    }
    return a;
}