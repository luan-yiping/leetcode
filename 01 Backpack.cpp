#include<iostream>
#include<vector>
using namespace std;
// 二维dp
void test()
{
	vector<int> weight = { 1,3,4 };
    
	vector<int> value = {15,20,30};
	int bagweight = 4;
	vector<vector<int>> dp(weight.size(), vector<int>(bagweight + 1,0));
	for (int i = bagweight; i >= weight[0]; i--)
	{
		dp[0][i] = value[0];
	}
	for (int i = 1; i < weight.size(); i++)
	{
		for (int j = 1; j <= bagweight; j++)
		{
			if (j < weight[i])
				dp[i][j] = dp[i - 1][j];
			else
				dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - weight[i]] + value[i]);
		}
	}
	cout << dp[weight.size() - 1][bagweight] << endl;

}

// 滚动数组一维dp
void test2()
{
	vector<int> weight = { 1,3,4 };
	vector<int> value = { 15,20,30 };
	int bagweight = 4;
	vector<int> dp(bagweight + 1, 0);
	// 遍历物品
	for (int i = 0; i < weight.size(); i++)
	{
		// 遍历背包
		for (int j = bagweight; j >= weight[i]; j--)
		{
			dp[j] = max(dp[j], dp[j - weight[i]] + value[i]);
		}
	}
	cout <<"一维 :"<< dp[bagweight] << endl;
}
int main()
{
	test();
	test2();
}