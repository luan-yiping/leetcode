// 设置哑结点(带哨兵位的头结点)处理从第一个结点反转的情况
// 将要反转的第一个结点后的结点头插
typedef struct ListNode ListNode;
struct ListNode* reverseBetween(struct ListNode* head, int left, int right)
{
    if (head == NULL)
    {
        return NULL;
    }
    ListNode* dummyHead = (ListNode*)malloc(sizeof(ListNode));
    dummyHead->next = head;
    ListNode* cur = head, * guard = dummyHead, * next = cur->next;
    int sub = right - left;
    left -= 1;
    while (left--)
    {
        guard = cur;
        cur = cur->next;
        next = cur->next;
    }
    while (sub--)
    {
        cur->next = next->next;
        next->next = guard->next;
        guard->next = next;
        next = cur->next;
    }
    head = dummyHead->next;
    free(dummyHead);
    return head;
}