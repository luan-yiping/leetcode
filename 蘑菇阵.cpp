#include<bits/stdc++.h>
using namespace std;
int has[25][25];
double dp[25][25];
int main()
{
    int n, m, k;
    while (cin >> n >> m >> k)
    {
        memset(has, 0, sizeof has);
        memset(dp, 0, sizeof dp);
        while (k--)
        {
            int x, y;
            cin >> x >> y;
            has[x][y] = 1;
        }
        for (int i = 1; i <= n; i++)
        {
            for (int j = 1; j <= m; j++)
            {
                if (i == 1 && j == 1) { dp[i][j] = 1; continue; }
                if (has[i][j]) { dp[i][j] = 0; continue; }
                if (i == n && j == m) {
                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
                    continue;
                }
                if (i == n) {
                    dp[i][j] = dp[i - 1][j] * 0.5 + dp[i][j - 1];
                    continue;
                }
                if (j == m) {
                    dp[i][j] = dp[i][j - 1] * 0.5 + dp[i - 1][j];
                    continue;
                }
                if (i == 1) {
                    dp[i][j] = dp[i][j - 1] * 0.5;
                    continue;
                }
                if (j == 1) {
                    dp[i][j] = dp[i - 1][j] * 0.5;
                    continue;
                }
                dp[i][j] = dp[i - 1][j] * 0.5 + dp[i][j - 1] * 0.5;
            }
        }
        printf("%.2lf\n", dp[n][m]);
    }
}