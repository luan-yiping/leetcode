// 计算长度差，让长链表先走长度差步，再让长短链表同时走，结点相同时就是我们要找的结点
typedef struct ListNode ListNode;
struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB)
{
    int lenA = 0, lenB = 0;
    ListNode* curA = headA, * curB = headB;
    while (curA != NULL)
    {
        lenA++;
        curA = curA->next;
    }
    while (curB != NULL)
    {
        lenB++;
        curB = curB->next;
    }
    int sub = abs(lenA - lenB);
    
    while (sub--)
    {
        if (lenB > lenA)
        {
            headB = headB->next;
        }
        else
        {
            headA = headA->next;
        }
    }
    while (headA != headB)
    {
        headA = headA->next;
        headB = headB->next;
    }
    return headA;
}