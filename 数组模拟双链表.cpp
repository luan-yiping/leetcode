#include<iostream>
using namespace std;
const int N = 100010;
int e[N], l[N], r[N], idx;
void init()
{
    r[0] = 1;
    l[1] = 0;
    idx = 2;
}
// 在k的右边插入x
void insert(int k, int x)
{
    e[idx] = x;

    r[idx] = r[k];
    l[idx] = k;

    l[r[k]] = idx;
    r[k] = idx;

    idx++;
}
void remove(int k)
{
    r[l[k]] = r[k];
    l[r[k]] = l[k];
}
int main()
{
    int m;
    cin >> m;
    init();
    while (m--)
    {
        int k, x;
        string op;
        cin >> op;
        // 链表最左端插入数x
        if (op == "L")
        {
            cin >> x;
            insert(0, x);
        }
        // 链表最右端插入数x
        else if (op == "R")
        {
            cin >> x;
            insert(l[1], x);
        }
        // 第k个插入的数删除
        else if (op == "D")
        {
            cin >> k;
            remove(k + 1);
        }
        // 第k个插入的数左侧插入
        else if (op == "IL")
        {
            cin >> k >> x;
            insert(l[k + 1], x);
        }
        // 第k个插入的数右侧输入
        else
        {
            cin >> k >> x;
            insert(k + 1, x);
        }
    }
    for (int i = r[0]; i != 1; i = r[i]) printf("%d ", e[i]);
}