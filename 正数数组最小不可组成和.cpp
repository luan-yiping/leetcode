class Solution
{
public:
    void dfs(vector<int>& arr, vector<int>& ans, int count, int& sum,
        int& min, int& max, int len)
    {
        if (count == len) return;
        for (int i = count; i < len; i++)
        {
            sum += arr[i];
            if (sum < min) min = sum;
            if (sum > max) max = sum;
            ans.push_back(sum);
            dfs(arr, ans, i + 1, sum, min, max, len);
            sum -= arr[i];
        }
    }
    int getFirstUnFormedNum(vector<int> arr, int len)
    {
        vector<int> ans;
        int count = 0, sum = 0, min = INT_MAX, max = INT_MIN;
        dfs(arr, ans, count, sum, min, max, len);
        unordered_map<int, int> m;
        for (int i = 0; i < ans.size(); i++) m[ans[i]]++;
        for (int i = min; i <= max; i++)
        {
            if (m[i] == 0)
                return i;
        }
        return max + 1;
    }
};