class Solution {
public:
    int minSubArrayLen(int s, vector<int>& nums)
    {
        int ret = INT_MAX;
        for (int i = 0, j = 0, sum = 0; i < nums.size(); i++)
        {
            sum += nums[i];
            while (sum - nums[j] >= s) sum -= nums[j++];
            if (sum >= s) ret = min(ret, i - j + 1);
        }
        if (ret == INT_MAX) ret = 0;
        return ret;
    }
};