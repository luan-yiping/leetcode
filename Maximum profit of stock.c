int max(int x, int y)
{
    return (x > y) ? x : y;
}
int min(int x, int y)

{
    return (x < y) ? x : y;
    
}
int maxProfit(int* prices, int pricesSize)
{
    
    if (pricesSize == 0)
        return 0;
    int profit = 0, cost = prices[0], i = 0;
    for (i = 0; i < pricesSize; i++)
    {
        cost = min(cost, prices[i]);
        profit = max(profit, prices[i] - min(cost, prices[i]));
    }
    return profit;
}