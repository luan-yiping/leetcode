/*三指针法
        prev指向头结点,cur指向第一个结点，next指向第二个结点
        当next->val和cur->val相同时让next向后移动，直到next为空或next->val != cur->val终止
        若cur->next != next说明存在重复元素,让cur不断移动直到等于next,在移动过程中,使prev->next指向cur->next,并将cur所指结点释放掉
        若cur->next == next说明不存在重复元素,让三指针分别向后移动即可
*/
class Solution
{
public:
    ListNode* deleteDuplication(ListNode* pHead)
    {
        if (pHead == NULL || pHead->next == NULL)
            return pHead;
        ListNode* dummyHead = (ListNode*)malloc(sizeof(ListNode));
        dummyHead->next = pHead;
        ListNode* prev = dummyHead, * cur = pHead, * next = cur->next;
        while (next)
        {
            while (next && next->val == cur->val)
            {
                next = next->next;
            }
            if (cur->next != next)
            {
                while (cur != next)
                {
                    prev->next = cur->next;
                    free(cur);
                    cur = prev->next;
                }
                if (cur)
                {
                    next = cur->next;
                }
            }
            else
            {
                prev = cur;
                cur = next;
                next = cur->next;
            }
        }
        pHead = dummyHead->next;
        free(dummyHead);
        
        return pHead;
    }
};