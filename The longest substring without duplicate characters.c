// ��������
int lengthOfLongestSubstring(char* s)
{
    int len = strlen(s);
    if (len <= 1)
    {
        return len;
    }
    int flag[128] = { 0 };
    int left = 0;
    int right = 0;
    int max = 0;
    while (right < len)
    {
        while (flag[s[right]] == 1)
        {
            flag[s[left++]] = 0;
        }
        flag[s[right++]] = 1;
        if (right - left > max)
        {
            max = right - left;
        }
    }
    return max;
}