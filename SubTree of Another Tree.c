typedef struct TreeNode TreeNode;
// 判断两树是否相同

bool isSame(TreeNode* root, TreeNode* subRoot)
{
    if (root == NULL && subRoot == NULL)
        return true;
    if (root == NULL || subRoot == NULL)
        return false;
    if (root->val != subRoot->val)
        return false;
    return isSame(root->left, subRoot->left) && isSame(root->right, subRoot->right);
}
// 分别以判断root的各个结点为根是否和子树subRoot相同
bool isSubtree(struct TreeNode* root, struct TreeNode* subRoot)
{
    if (subRoot == NULL)
        return true;
    if (root == NULL)
        return false;
    return isSame(root, subRoot) || isSubtree(root->left, subRoot) || isSubtree(root->right, subRoot);
}