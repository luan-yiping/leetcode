typedef struct Node Node;
/*
第一步 : 将复制出来的结点连接到原链表的后面
如: 7-7(copy)-13-13(copy)-11-11(copy)-10-10(copy)-1-1(copy)-NULL
第二步 : 调整复制结点的random指针
第三步 : 将复制结点从原链表上分离下来
*/
struct Node* copyRandomList(struct Node* head)
{
    if (head == NULL)
        return NULL;
    // 第一步
    Node* cur = head;
    while (cur)
    {
        Node* copy = (Node*)malloc(sizeof(Node));
        copy->val = cur->val;
        copy->next = cur->next;
        cur->next = copy;
        cur = copy->next;
    }
    // 第二步
    cur = head;
    while (cur)
    {
        Node* copy = cur->next;
        if (cur->random == NULL)
            copy->random = NULL;
        else
            copy->random = cur->random->next;
        cur = copy->next;
    }
    // 第三步
    cur = head;
    Node* newHead = cur->next, * newTail = cur->next;
    cur = newHead->next;
    while (cur)
    {
        newTail->next = cur->next;
        newTail = newTail->next;
        cur = newTail->next;
    }
    return newHead;
}