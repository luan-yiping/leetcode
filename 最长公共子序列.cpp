#include<iostream>
#include<vector>
using namespace std;
int main()
{
    string s1, s2;
    int len1, len2;
    while (cin >> s1 >> s2 >> len1 >> len2)
    {
        vector<int> dp(len2 + 1);
        int sum = 0;
        for (int i = 1; i <= len2; i++)
        {
            if (i == 1)
            {
                dp[i] = s2[i - 1] - s1[i - 1];
                continue;
            }
            dp[i] = (dp[i - 1] * 26) % 1000007;
            if (i <= s1.size()) dp[i] = dp[i] - (s1[i - 1] - 'a' + 1);
            if (i <= s2.size()) dp[i] = dp[i] + (s2[i - 1] - 'a');
            if (i >= len1) sum += dp[i];
        }
        cout << (sum) % 1000007 << endl;
    }
}