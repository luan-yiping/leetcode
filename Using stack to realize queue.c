#define INIT_CAPACITY 4
typedef int STDataType;
typedef struct Stack
{
	STDataType* a;
	int top;  // 栈顶下标
	int capacity; // 栈容量
}Stack;
// 判断栈是否为空，为空返回true,非空返回false
bool StackEmpty(Stack* pst)
{
	assert(pst);
	return pst->top == 0;
}
// 栈的初始化
void StackInit(Stack* pst)
{
	assert(pst);
	pst->a = (STDataType*)malloc(sizeof(STDataType) * INIT_CAPACITY);
	if (pst->a == NULL)
	{
		printf("malloc failed\n");
		exit(-1);
	}
	pst->top = 0;
	pst->capacity = INIT_CAPACITY;
}
// 入栈
void StackPush(Stack* pst, STDataType x)
{
	assert(pst);
	// 动态增容
	if (pst->top == pst->capacity)
	{
		pst->capacity *= 2;
		STDataType* tmp = (STDataType*)realloc(pst->a, sizeof(STDataType) * pst->capacity);
		if (tmp == NULL)
		{
			printf("realloc failed\n");
			exit(-1);
		}
		pst->a = tmp;
	}
	pst->a[pst->top] = x;
	pst->top++;
}
// 出栈
void StackPop(Stack* pst)
{
	assert(pst);
	assert(!StackEmpty(pst));
	pst->top--;
}
// 获取栈的元素个数
int StackSize(Stack* pst)
{
	assert(pst);
	return pst->top;
}
// 销毁栈
void StackDestroy(Stack* pst)
{
	assert(pst);
	pst->top = pst->capacity = 0;
	free(pst->a);
	pst->a = NULL;
}
// 获取栈顶元素
int StackTop(Stack* pst)
{
	assert(pst);
	return pst->a[pst->top - 1];
}

typedef struct {
	Stack PushST;
	Stack PopST;
} MyQueue;



MyQueue* myQueueCreate() {
	MyQueue* obj = (MyQueue*)malloc(sizeof(MyQueue));
	StackInit(&obj->PushST);
	StackInit(&obj->PopST);
	return obj;
}

// 将数据入到 PushST 栈中
void myQueuePush(MyQueue* obj, int x) {
	StackPush(&obj->PushST, x);
}

// 若PopST栈中数据为空,将PushST栈中的数据转移到PopST中,返回PopST的栈顶元素,再删除掉栈顶元素
int myQueuePop(MyQueue* obj)
{
	if (StackEmpty(&obj->PopST))
	{
		while (!StackEmpty(&obj->PushST))
		{
			StackPush(&obj->PopST, StackTop(&obj->PushST));
			StackPop(&obj->PushST);
		}
	}
	int ans = StackTop(&obj->PopST);
	StackPop(&obj->PopST);
	return ans;
}

// 若PopST栈中数据为空,将PushST栈中的数据转移到PopST中,返回PopST的栈顶元素
int myQueuePeek(MyQueue* obj)
{
	if (StackEmpty(&obj->PopST))
	{
		while (!StackEmpty(&obj->PushST))
		{
			StackPush(&obj->PopST, StackTop(&obj->PushST));
			StackPop(&obj->PushST);
		}
	}
	return StackTop(&obj->PopST);
}

// 两栈都为空,才为空
bool myQueueEmpty(MyQueue* obj) {
	return StackEmpty(&obj->PushST) && StackEmpty(&obj->PopST);
}

// 先释放两个栈,再释放MyQueue结点
void myQueueFree(MyQueue* obj) {
	StackDestroy(&obj->PushST);
	StackDestroy(&obj->PopST);
	free(obj);
}