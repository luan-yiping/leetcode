class Solution {
public:
    vector<vector<int>> merge(vector<vector<int>>& intervals)
    {
        vector<vector<int>> ret;
        if (intervals.empty())  return ret;
        sort(intervals.begin(), intervals.end());
        int left = intervals[0][0], right = intervals[0][1];
        for (int i = 1; i < intervals.size(); i++)
        {
            if (intervals[i][0] > right)
            {
                ret.push_back({ left,right });
                left = intervals[i][0], right = intervals[i][1];
            }
            else
            {
                right = max(right, intervals[i][1]);
            }
        }
        ret.push_back({ left,right });
        return ret;
    }
};