class Solution {
public:
    stack<int> num;
    stack<char> op;

    void eval()
    {
        char c = op.top();
        op.pop();
        int a = num.top();
        num.pop();
        int b = num.top();
        num.pop();

        if (c == '*') num.push(a * b);
        if (c == '+') num.push(a + b);
        if (c == '/') num.push(b / a);
        if (c == '-') num.push(b - a);
    }

    int calculate(string s)
    {
        unordered_map<char, int> pr;
        pr['+'] = pr['-'] = 0, pr['*'] = pr['/'] = 1;

        for (int i = 0; i < s.size(); i++)
        {
            if (s[i] == ' ') continue;
            if (isdigit(s[i]))
            {
                int x = 0, j = i;
                while (j < s.size() && isdigit(s[j])) x = x * 10 + (s[j++] - '0');
                num.push(x);
                i = j - 1;
            }
            else
            {
                while (op.size() && pr[op.top()] >= pr[s[i]]) eval();
                op.push(s[i]);
            }
        }

        while (op.size()) eval();

        return num.top();
    }
};