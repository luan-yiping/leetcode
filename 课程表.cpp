class Solution {
public:
    bool canFinish(int numCourses, vector<vector<int>>& p) {
        // 入度
        vector<int> input(numCourses, 0);
        // 课程号->依赖于该课程的课程数组
        unordered_map<int, vector<int>> map;

        for (int i = 0; i < p.size(); i++)
        {
            input[p[i][0]]++;
            map[p[i][1]].push_back(p[i][0]);
        }

        // 将入度为0的点入队
        queue<int> q;
        for (int i = 0; i < input.size(); i++)
        {
            if (input[i] == 0) q.push(i);
        }

        int cnt = 0;
        while (q.size())
        {
            int select = q.front();
            q.pop();
            cnt++;
            vector<int> v = map[select];
            if (v.size())
            {
                for (auto& e : v)
                {
                    if (--input[e] == 0)
                    {
                        q.push(e);
                    }
                }
            }
        }
        if (cnt == numCourses) return true;
        else return false;
    }
};