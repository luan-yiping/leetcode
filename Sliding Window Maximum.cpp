class MonotonicQueue
{
private:
    deque<int> deq;
public:
    void push(int n)
    {
        while (!deq.empty() && deq.back() < n)
            deq.pop_back();
        deq.push_back(n);
    }
    void pop(int n)
    {
        if (!deq.empty() && deq.front() == n)
            deq.pop_front();
    }
    int max()
    {
        return deq.front();
    }
};
class Solution
{
public:
    vector<int> maxSlidingWindow(vector<int>& nums, int k)
    {
        vector<int>ret;
        MonotonicQueue deq;
        for (int i = 0; i < k; i++)
        {
            deq.push(nums[i]);
        }
        ret.push_back(deq.max());
        for (int i = k; i < nums.size(); i++)
        {
            deq.pop(nums[i - k]);
            deq.push(nums[i]);
            ret.push_back(deq.max());
        }
        return ret;
    }
};
