/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
 
class Solution
{
public:
    ListNode* hasCycle(ListNode* head)
    {
        auto s = head, f = head;
        while (1)
        {
            if (!f) return nullptr;
            f = f->next;
            s = s->next;
            if (!f) return nullptr;
            f = f->next;
            if (f == s) return f;
        }
    }
    ListNode* detectCycle(ListNode* head)
    {
        auto meet = hasCycle(head);
        if (meet == nullptr) return nullptr;
        else
        {
            auto a = head, b = meet;
            while (a != b)
            {
                a = a->next;
                b = b->next;
            }
            return a;
        }
    }
};