int* twoSum(int* nums, int numsSize, int target, int* returnSize)
{
    
    int* ret = (int*)malloc(sizeof(int) * 2);
    int start = 0, end = numsSize - 1;
    while (start < end)
    {
        if (nums[start] + nums[end] > target)
        {
            end--;
        }
        else if (nums[start] + nums[end] < target)
        {
            start++;
        }
        else
        {
            ret[0] = nums[start];
            ret[1] = nums[end];
            break;
        }
    }
    if (start == end)
    {
        *returnSize = 0;
        return NULL;
    }
    *returnSize = 2;
    
    return ret;

}