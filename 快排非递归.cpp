#include <iostream>
#include<vector>
#include<stack>
#include<algorithm>
using namespace std;
class Solution
{
public:
    int partition(vector<int>& nums, int l, int r)
    {
        int i = l - 1, j = r + 1, x = nums[(l + r) >> 1];
        while (i < j)
        {
            do i++; while (nums[i] < x);
            do j--; while (nums[j] > x);
            if (i < j) swap(nums[i], nums[j]);
        }
        return j;
    }
    vector<int> sortArray(vector<int>& nums)
    {
        int start = 0, end = nums.size() - 1;
        stack<int> st;
        st.push(end);
        st.push(start);
        while (!st.empty())
        {
            int l = st.top();
            st.pop();
            int r = st.top();
            st.pop();
            int index = partition(nums, l, r);
            if (l < index)
            {
                st.push(index);
                st.push(l);
            }
            if (index + 1 < r)
            {
                st.push(r);
                st.push(index + 1);
            }
        }
        return nums;
    }
};
int main()
{
    vector<int> nums{ 5,1,2,0,0 };
    Solution().sortArray(nums);
}





