class Solution
{
public:

    vector<vector<int>> generateMatrix(int n)
    {
        vector<vector<int>> res(n, vector<int>(n));
        vector<vector<bool>> st(n, vector<bool>(n));
        int dx[] = { 0,1,0,-1 }, dy[] = { 1,0,-1,0 };
        for (int i = 0, x = 0, y = 0, d = 0; i < n * n; i++)
        {
            res[x][y] = i + 1;
            st[x][y] = true;
            int a = x + dx[d], b = y + dy[d];
            if (a < 0 || a >= n || b < 0 || b >= n || st[a][b])
            {
                d = (d + 1) % 4;
                a = x + dx[d], b = y + dy[d];
            }
            x = a, y = b;
        }
        return res;
    }
};