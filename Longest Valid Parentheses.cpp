class Solution
{
public:
    int longestValidParentheses(string s)
    {

        //用栈模拟一遍，将所有无法匹配的括号的位置全部置1
        //例如: "()(()"的mark为[0, 0, 1, 0, 0]
        //再例如: ")()((())"的mark为[1, 0, 0, 1, 0, 0, 0, 0]
        //经过这样的处理后, 此题就变成了寻找最长的连续的0的长度
        stack<int> st;
        
        vector<int> mask(s.size(),0);
        for(size_t i = 0;i < s.size();i++)
        {
            if(s[i] == '(')
            {
                st.push(i);
                
            }
            else
            {
                // 未匹配的右括号
                if(st.empty())
                    mask[i] = 1;
                else
                    st.pop();
            }
        }
        // 未匹配的左括号
        while(!st.empty())
        {
            mask[st.top()] = 1;
            st.pop();
        }
        int len = 0,ans = 0;
        for(size_t i = 0;i < s.size();i++)
        {
            if(mask[i] == 1)
            {
                len = 0;
                continue;
            }
            len++;
            ans = max(len,ans);
        }
        return ans;
    }
        // dp
        vector<int> dp(s.size(), 0);
        int ans = 0;
        for (int i = 1; i < s.size(); i++)
        {
            if (s[i] == ')')
            {
                int len = i - dp[i - 1] - 1;
                //if(i - dp[i - 1] - 2 >= 0 && s[i - dp[i - 1] - 1] == '(')
                    //dp[i] = 2 + dp[i - 1] + dp[i - dp[i - 1] - 2];
                //else if(i - dp[i - 1] - 2 < 0 && i - dp[i - 1] - 1 >= 0 && s[i - dp[i - 1] - 1] == '(')
                   // dp[i] = 2 + dp[i - 1];

                if (len >= 0 && s[len] == '(')
                {
                    dp[i] = (len - 1 >= 0) ? 2 + dp[i - 1] + dp[len - 1] : 2 + dp[i - 1];
                }
                ans = max(ans, dp[i]);
            }
        }
        return ans;
    }
};