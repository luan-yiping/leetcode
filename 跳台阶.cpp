#include<iostream>
#include<vector>
using namespace std;
int dp[100010];
void f(int x, vector<int>& v)
{
    for (int i = 2; i * i <= x; i++)
    {
        if (x % i == 0)
        {
            v.push_back(i);
            if (i != x / i)
                v.push_back(x / i);
        }
    }
}
int main()
{
    memset(dp, 0x3f, sizeof dp);
    int n, m;
    cin >> n >> m;
    dp[n] = 0;
    for (int i = n; i <= m;i++)
    {
        vector<int> v;
        f(i, v);
        for (int j = 0; j < v.size(); j++)
        {
            if(i + v[j] <= m)
                dp[i + v[j]] = min(dp[i] + 1, dp[i + v[j]]);
        }
    }
    if (dp[m] == 0x3f3f3f3f) cout << -1 << endl;
    else cout << dp[m] << endl;
}