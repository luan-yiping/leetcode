class Solution
{
public:
    bool isBalanced(TreeNode* root)
    {
        // return dfs(root) >= 0;
        stack<TreeNode*> st;
        unordered_map<TreeNode*, int> map;
        TreeNode* pre = nullptr;
        while (st.size() || root)
        {
            while (root)
            {
                st.push(root);
                root = root->left;
            }
            root = st.top();
            // 左节点回溯过来的
            if (root->right && pre != root->right)
            {
                root = root->right;
            }
            else
            {
                st.pop();
                int left = root->left ? map[root->left] : 0;
                int right = root->right ? map[root->right] : 0;
                if (abs(left - right) > 1) return false;
                map[root] = max(left, right) + 1;
                pre = root;
                root = nullptr;
            }
        }
        return true;
    }
    /*
    int dfs(TreeNode* root)
    {
        if(root == nullptr) return 0;
        int left = dfs(root->left),right = dfs(root->right);
        if(left >= 0 && right >= 0 && abs(right - left) <= 1) return max(left,right) + 1;
        else return -1;
    }
    */
};