#include<iostream>
#include<cstring>
using namespace std;
const int N = 100010, M = N * 2;
int h[N], e[M], ne[M], idx, ans = N;
int n;
bool st[N];
void add(int a, int b)
{
    e[idx] = b;
    ne[idx] = h[a];
    h[a] = idx++;
}
// 返回以u为根节点的子树的节点数量
int dfs(int u)
{
    st[u] = true;
    // res为删除u节点后，所有连通块中节点数的最大值
    int sum = 1, res = 0;
    for (int i = h[u]; i != -1; i = ne[i])
    {
        // 节点标号
        int j = e[i];
        if (!st[j])
        {
            int s = dfs(j);
            res = max(res, s);
            sum += s;
        }
    }
    res = max(res, n - sum);
    ans = min(res, ans);
    return sum;
}
int main()
{
    memset(h, -1, sizeof h);
    scanf("%d", &n);
    for (int i = 0; i < n - 1; i++)
    {
        int a, b;
        scanf("%d%d", &a, &b);
        add(a, b);
        add(b, a);
    }
    dfs(1);
    printf("%d", ans);
}