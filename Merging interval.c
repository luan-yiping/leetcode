int cmp(const void* e1, const void* e2)
{
    int* ans1 = *(int**)e1, * ans2 = *(int**)e2;
    return ans1[0] - ans2[0];
}
int** merge(int** intervals, int intervalsSize, int* intervalsColSize, int* returnSize, int** returnColumnSizes)
{
    // 按照区间左端点进行升序排序
    qsort(intervals, intervalsSize, sizeof(int*), cmp);
    int** ret = (int**)malloc(sizeof(int*) * intervalsSize);
    *returnColumnSizes = (int*)malloc(sizeof(int) * intervalsSize);
    for (int i = 0; i < intervalsSize; i++)
        (*returnColumnSizes)[i] = 2;
    ret[0] = (int*)malloc(sizeof(int) * 2);
    ret[0][0] = intervals[0][0];
    ret[0][1] = intervals[0][1];
    int count = 0;
    for (int i = 1; i < intervalsSize; i++)
    {
        // 当前区间的起点小于等于合并区间的终点且当前区间的终点大于等于合并区间的终点,合并
        if (intervals[i][0] <= ret[count][1] && intervals[i][1] >= ret[count][1])
            ret[count][1] = intervals[i][1];
        // 无需合并,重新开辟空间
        else if (intervals[i][0] > ret[count][1])
        {
            count++;
            ret[count] = (int*)malloc(sizeof(int) * 2);
            ret[count][0] = intervals[i][0];
            ret[count][1] = intervals[i][1];
        }
    }
    *returnSize = count + 1;
    return ret;
}