class Solution
{
public:
    vector<string> topKFrequent(vector<string>& words, int k)
    {
        map<string, int> countMap;
        for (auto& e : words)
        {
            countMap[e]++;
        }

        multimap<int, string, greater<int>> sortMap;
        for (auto& e : countMap)
        {
            sortMap.insert(make_pair(e.second, e.first));
        }

        vector<string> ans;
        auto it = sortMap.begin();
        while (k--)
        {
            ans.push_back(it->second);
            it++;
        }

        return ans;
    }
};