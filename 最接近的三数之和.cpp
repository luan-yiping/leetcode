#include <iostream>
#include<vector>
#include<algorithm>
using namespace std;
class Solution
{
public:
    int threeSumClosest(vector<int>& nums, int target)
    {
        sort(nums.begin(), nums.end());
        int ans = 0, sum = 0;
        for (int i = 0; i < nums.size(); i++)
        {
            int j = i + 1, k = nums.size() - 1;
            while (j < k)
            {
                sum = nums[i] + nums[j] + nums[k];
                if (sum > target)
                {
                    if (!i && !ans) ans = sum;
                    else if (abs(ans - target) > sum - target) ans = sum;
                    k--;
                }
                else if (sum < target)
                {
                    if (!i && !ans) ans = sum;
                    else if (abs(ans - target) > target - sum) ans = sum;
                    j++;
                }
                else {
                    ans = sum;
                    j++;
                    k--;
                }
            }
        }
        return ans;
    }
};
int main()
{
    vector<int> nums{0,2,1,-3};
    cout << Solution().threeSumClosest(nums, 1) << endl;
}