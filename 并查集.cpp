/*
并查集
解决的问题
(1).合并两个集合
(2).判断两个元素是否在同一集合当中
原理 : 每个集合用一棵树来表示，树根的编号为集合的编号，每个节点存储其父节点,p[x]表示x的父节点

(1). 判断树根 if(p[x] == x)
(2). 求x的集合编号 while(p[x] != x) x = p[x];
(3). 合并两个集合  p[x]为x的集合编号,p[y]为y的集合编号,p[x] = y
*/

#include<iostream>
using namespace std;
const int N = 100010;
int p[N];
// 求编号为x节点的祖宗节点 + 路径压缩
int find(int x)
{
    if (p[x] != x) p[x] = find(p[x]);
    return p[x];
}
int main()
{
    int n, m;
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= n; i++) p[i] = i;
    while (m--)
    {
        char op[2];
        int a, b;
        scanf("%s%d%d", op, &a, &b);
        if (op[0] == 'M') p[find(a)] = find(b);
        else
        {
            if (find(a) == find(b)) printf("Yes\n");
            else printf("No\n");
        }
    }
}