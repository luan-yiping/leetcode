#include<iostream>
using namespace std;
const int N = 100010, M = 1000010;
char s[M], p[N];
int ne[N];
int n, m;
int main()
{
    cin >> n >> p + 1 >> m >> s + 1;
    // next
    for (int i = 2; i <= n; i++)
    {
        int k = ne[i - 1];
        while (k && p[k + 1] != p[i]) k = ne[k];
        if (p[k + 1] == p[i]) k++;
        ne[i] = k;
    }
    // kmp
    for (int i = 1, j = 0; i <= m; i++)
    {
        while (j && s[i] != p[j + 1]) j = ne[j];
        if (s[i] == p[j + 1]) j++;
        if (j == n)
        {
            printf("%d ", i - n);
            j = ne[j];
        }
    }
}