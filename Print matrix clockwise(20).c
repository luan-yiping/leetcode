int* spiralOrder(int** matrix, int matrixSize, int* matrixColSize, int* returnSize)
{
	if (matrixSize == 0)
	{
		*returnSize = 0;
		return NULL;
	}
	int* ret = (int*)malloc(sizeof(int) * matrixSize * (*matrixColSize));
	*returnSize = matrixSize * (*matrixColSize);
	int l = 0, r = (*matrixColSize) - 1, t = 0, b = matrixSize - 1;
	int i = 0, x = 0;
	while (1)
    
	{
		for (i = l; i <= r; i++)
		{
			ret[x++] = matrix[l][i];
		}
		if (++t > b)
			break;
		for (i = t; i <= b; i++)
		{
			ret[x++] = matrix[i][r];
		}
		if (l > --r)
			break;
		for (i = r; i >= l; i--)
		{
			ret[x++] = matrix[b][i];
		}
		if (t > --b)
			break;
		for (i = b; i >= t; i--)
		{
			ret[x++] = matrix[i][l];
		}
		if (++l > r)
			break;
	}
	return ret;
}