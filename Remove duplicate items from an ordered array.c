int removeDuplicates(int* nums, int numsSize)
{
    if (numsSize == 0)
    {
        return 0;
    }
    int cur = 1, prev = 0, ret = 1;
    while (cur < numsSize)
    {
        if (nums[cur] != nums[prev])
        {
            nums[ret] = nums[cur];
            ret++;
        }
        cur++;
        prev++;
    }
    return ret;
}