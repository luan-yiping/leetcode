#define INIT_CAPACITY 4
typedef int STDataType;
typedef struct Stack
{
	STDataType* a;
	int top;  // 栈顶下标
	int capacity; // 栈容量
}Stack;
// 判断栈是否为空，为空返回true,非空返回false
bool StackEmpty(Stack* pst)
{
	assert(pst);
	return pst->top == 0;
}
// 栈的初始化
void StackInit(Stack* pst)
{
	assert(pst);
	pst->a = (STDataType*)malloc(sizeof(STDataType) * INIT_CAPACITY);
	if (pst->a == NULL)
	{
		printf("malloc failed\n");
		exit(-1);
	}
	pst->top = 0;
	pst->capacity = INIT_CAPACITY;
}
// 入栈
void StackPush(Stack* pst, STDataType x)
{
	assert(pst);
	// 动态增容
	if (pst->top == pst->capacity)
	{
		pst->capacity *= 2;
		STDataType* tmp = (STDataType*)realloc(pst->a, sizeof(STDataType) * pst->capacity);
		if (tmp == NULL)
		{
			printf("realloc failed\n");
			exit(-1);
		}
		pst->a = tmp;
	}
	pst->a[pst->top] = x;
	pst->top++;
}
// 出栈
void StackPop(Stack* pst)
{
	assert(pst);
	assert(!StackEmpty(pst));
	pst->top--;
}
// 获取栈顶元素
STDataType StackTop(Stack* pst)
{
	assert(pst);
	assert(!StackEmpty(pst));
	return pst->a[pst->top - 1];
}
// 获取栈的元素个数
int StackSize(Stack* pst)
{
	assert(pst);
	return pst->top;
}
// 销毁栈
void StackDestroy(Stack* pst)
{
	assert(pst);
	pst->top = pst->capacity = 0;
	free(pst->a);
	pst->a = NULL;
}
// 遇到左括号入栈,遇到右括号和栈顶元素比较,若匹配，让栈顶元素出栈，若不匹配，直接返回false
// s走到空以后，若栈里没有元素，返回true,有元素返回false
// 记得在返回之前销毁栈,防止内存泄漏
bool isValid(char* s)
{
	Stack st;
	StackInit(&st);
	while (*s)
	{
		if (*s == '(' || *s == '[' || *s == '{')
			StackPush(&st, *s);
		else
		{
			if (StackSize(&st) == 0)
			{
				// 记得销毁栈，防止内存泄漏
				StackDestroy(&st);
				return false;
			}
			if (StackTop(&st) == '(' && *s != ')'
				|| StackTop(&st) == '[' && *s != ']'
				|| StackTop(&st) == '{' && *s != '}'
				)
			{
				StackDestroy(&st);
				return false;
			}
			else
			{
				StackPop(&st);
			}
		}
		++s;
	}
	bool ret = StackSize(&st) == 0 ? true : false;
	StackDestroy(&st);
	return ret;


}