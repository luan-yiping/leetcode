class Solution
{
public:
    bool isPalindrome(string& s, int startindex, int i)
    {
        int left = startindex, right = i;
        while (left < right)
        {
            if (s[left] != s[right])
                return false;
            left++;
            right--;
        }
        return true;
    }
    void backtrack(vector<vector<string>>& res, vector<string>& path, string& s, int startindex)
    {
        if (startindex == s.size())
        {
            res.push_back(path);
            return;
        }
        for (int i = startindex; i < s.size(); i++)
        {
            if (isPalindrome(s, startindex, i))
            {
                path.push_back(s.substr(startindex, i - startindex + 1));
            }
            else
            {
                continue;
            }
            backtrack(res, path, s, i + 1);
            path.pop_back();
        }
    }
    vector<vector<string>> partition(string s)
    {
        vector<vector<string>> res;
        vector<string> path;
        backtrack(res, path, s, 0);
        return res;
    }
};